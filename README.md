## EOTU路由器与服务器通信协议商定

>   SERVERIP: 服务器IP地址。
>   192.168.10.1 ：	路由器IP地址。

### 一、	路由器心跳：
	路由器发送：
>
<pre><code>
[GET /hello/ping.php/?gw_id=router_ID&sys_uptime=590&sys_memfree=92364&sys_load=0.08&wifidog_uptime=21256903&ipinfo={
        "192.168.10.224":       {
                "mac":  "e0:a3:ac:86:2c:22",
                "token":        "1",
                "incoming":     886473,
                "incoming_history":     0,
                "outgoing":     188145,
                "outgoing_history":     0
      	}
    } HTTP/1.0
    User-Agent: WiFiDog 1.3.0
    Host: SERVERIP
]
</code></pre>

	服务器只需要回复一个含 Pong 字符的数据即可，并处理ipinfo后带的ip,mac 流量等信息
>   <?php 
	    echo "Pong"; 
    ?>

### 二、路由器监控连接IP断开连接（间隔=心跳时间X5，可设置）：
    路由器发现设定间隔时间内IP无法ping上，将发送给服务器是否断开此IP。
    路由器发送：
>   
<pre><code>
[GET /hello/auth.php/?stage=counters&ip=192.168.10.224&mac=e0:a3:ac:86:2c:22&token=1&incoming=898855&outgoing=200647&gw_id=router_ID HTTP/1.0
User-Agent: WiFiDog 1.3.0
Host: SERVERIP
]
</code></pre>

	服务器回复：
>   
<pre><code>
[HTTP/1.1 200 OK
Date: Fri, 03 Mar 2017 06:31:57 GMT
Server: Apache/2.4.18 (Ubuntu)
Content-Length: 7
Connection: close
Content-Type: text/html; charset=UTF-8
Auth: 1]
</code></pre>
	即<?php echo "Auth: 1" ?>


### 三、新用户登入路由器，或者用浏览器访问任意网址
#####	step 1 :	路由器让用户跳转，用户将登入：
>   
<pre><code>
	 http://SERVERIP:80/hello/login.php/?gw_address=192.168.10.1&gw_port=2060&gw_id=router_ID&ip=192.168.10.224&mac=e0:a3:ac:86:2c:22&url=http://www.baidu.com
</code></pre>	 

#####	step 2 :	服务器回复给用户简例（携带TOKEN值）：
>   
  		<a href=“http://192.168.10.1:2060/wifidog/auth?token=1&url=http://www.baidu.com”>> 
            请点此登入服务器授权 <?php echo $auth_url ?>
        </a>

######        step 2.1	用户（携带TOKEN）点击转入路由器后，路由器（携带TOKEN）回复给服务器：
>   
<pre><code>
[GET /hello/auth.php/?stage=login&ip=192.168.10.224&mac=e0:a3:ac:86:2c:22&token=1&incoming=0&outgoing=0&gw_id=router_ID HTTP/1.0
User-Agent: WiFiDog 1.3.0
Host: SERVERIP
]
</code></pre>
		
###### 		step 2.2 ：	服务器回复Auth: 1 ，代表允许用户登入。
>   
		<?php echo "Auth: 1" ?>


###### 		step 2.3 :	路由器收到后将允许此用户上网，并让用户登入服务器portal广告界面：
> 
			http://192.168.10.167:80/hello/portal.php/?gw_id=router_ID&url=\http:/qq.com/
		
###### 		step 2.4 :	服务器处理广告界面，可以让用户进入路由器/files/界面下载文件。
>   
		<h2 color="red">国盛广告：balabala</h2>
		<a href="http://192.168.10.1/files/"> 你已经授权了，点击登入路由器 下载视频：</a>
		
### 四、路由器心跳后，服务器返回设置信息。
	同流程一，路由器请求“GET /hello/ping.php/?”，服务器在Pong字符串后添加 json格式数据就行：
>   
	<?php
        $file_path = "function_test.json";
        if(file_exists($file_path)){
            $str = file_get_contents($file_path);//将整个文件内容读入到一个字符串中
            $str = str_replace("\r\n","<br />",$str);
        }else{
            echo "file[$file_path] not exist";
        }  
    echo "Pong:".$str;

### 五、服务器在路由器局域网内主动向路由器请求信息：
	具体字段，相见<function_test.json>
#####	1、	设置相关信息

######		例1，发送短信
>   
<pre><code>
POST /eotu/? HTTP/1.1
Host: 192.168.10.1:2060
Accept: */*
Content-Length: 164
Content-Type: application/x-www-form-urlencoded
Pong:{"set":[{"method":"send_message","paras":[{"端口号":"/dev/ttyUSB3"},{"手机号":"18279440205"},{"短信内容":"shen zhen router"}]}]}
</code></pre>

######		路由器回复
>   
		{ "set_result": { "send_message": { "send_message": "SMS ready to send" } } }

#####		例2，设置无线方式
> 
<pre><code>
POST /eotu/? HTTP/1.1
Host: 192.168.10.1:2060
Accept: */*
Content-Length: 353
Content-Type: application/x-www-form-urlencoded
Pong:{"set":[{"method":"wifi_ap_mode_set","paras":[{"test_1_SSID":"uci set wireless.apmode.ssid='Botu_GS'"},{"test_2_加密方式":"uci set wireless.apmode.encryption='none'"},{"test_3_密码":"uci set wireless.apmode.key='12345678'"},{"test_4_信号强度":"uci set wireless.radio0.txpower='20'"},{"test_5_导入（必备）":"uci commit wireless"}]}]}
</code></pre>
######		路由器回复：
		{ "set_result": { "wifi_ap_mode_set": { "wifi_ap_mode_set": "ok" } } }
		
#####    2、	请求路由器信息
		
######  例1, 路由器扫描周边wifi，方便做中继
>   
<pre><code>
POST /eotu/? HTTP/1.1
Host: 192.168.10.1:2060
Accept: */*
Content-Length: 97
Content-Type: application/x-www-form-urlencoded
Pong:{"request":[{"method":"iwinfo_wlan0_scan","paras":[{"test_1_命令":"iwinfo wlan0 scan"}]}]}
<code><pre>

######	路由器回复（格式没有做处理）：
>   
		{ "request_result": { "iwinfo_wlan0_scan": { "iwinfo_wlan0_scan": "(null)Cell 01 - Address: 14:75:90:E2:77:88\n ESSID: \"wx-s.net\"\n Mode: Master Channel: 6\n Signal: -62 dBm Quality: 48/70\n Encryption: mixed WPA/WPA2 PSK (TKIP, CCMP)\n\nCell 02 - Address: F4:6A:92:26:69:60\n ESSID: \"ljlshd\"\n Mode: Master Channel: 12\n Signal: -90 dBm Quality: 20/70\n Encryption: mixed WPA/WPA2 PSK (CCMP)\n\nCell 03 - Address: E4:D3:32:AE:79:F0\n ESSID: \"guosheng\"\n Mode: Master Channel: 1\n Signal: -84 dBm Quality: 26/70\n Encryption: mixed WPA/WPA2 PSK (TKIP, CCMP)\n\nCell 04 - Address: 40:16:9F:4D:09:06\n ESSID: \"TP-LINK_4D0906\"\n Mode: Master Channel: 1\n Signal: -88 dBm Quality: 22/70\n Encryption: mixed WPA/WPA2 PSK (CCMP)\n\nCell 05 - Address: 88:25:93:28:1B:F2\n ESSID: \"jtwd\"\n Mode: Master Channel: 6\n Signal: -94 dBm Quality: 16/70\n Encryption: mixed WPA/WPA2 PSK (CCMP)\n\nCell 06 - Address: 44:97:5A:A9:6B:72\n ESSID: \"FAST_6B72\"\n Mode: Master Channel: 6\n Signal: -94 dBm Quality: 16/70\n Encryption: mixed WPA/WPA2 PSK (CCMP)\n\nCell 07 - Address: E0:19:1D:04:68:68\n ESSID: \"HUAWEI-FLFJKT\"\n Mode: Master Channel: 6\n Signal: -76 dBm Quality: 34/70\n Encryption: mixed WPA/WPA2 PSK (TKIP, CCMP)\n\nCell 08 - Address: C8:3A:35:AF:80:71\n ESSID: unknown\n Mode: Master Channel: 4\n Signal: -94 dBm Quality: 16/70\n Encryption: WPA PSK (CCMP)\n\nCell 09 - Address: EA:D3:32:AE:79:F0\n ESSID: \"TP-LINK_IPTV_AE79F0\"\n Mode: Master Channel: 1\n Signal: -82 dBm Quality: 28/70\n Encryption: none\n\nCell 10 - Address: C8:3A:35:2E:A5:48\n ESSID: \"Tenda_2EA548\"\n Mode: Master Channel: 7\n Signal: -78 dBm Quality: 32/70\n Encryption: mixed WPA/WPA2 PSK (CCMP)\n\n" } } }
		
#####   3、	启动相关设置
		四个启动功能：eotu_wifidog重启和关闭，network重启，系统重启。
######  例1，wifidog启动
>   
<pre><code>
POST /eotu/? HTTP/1.1
Host: 192.168.10.1:2060
Accept: */*
Content-Length: 58
Content-Type: application/x-www-form-urlencoded
Pong:{"boot":[{"method":"wdctl_eotu_restart","paras":[]}]}
</code></pre>

######		例2,network重启
>   
<pre><code>
POST /eotu/? HTTP/1.1
Host: 192.168.10.1:2060
Accept: */*
Content-Length: 55
Content-Type: application/x-www-form-urlencoded
Pong:{"boot":[{"method":"network_restart","paras":[]}]}
</code></pre>


### 六、服务器Auth值定义
>   
<pre><code>
0 - AUTH_DENIED - User firewall users are deleted and the user removed.
1 - AUTH_ALLOWED - User was valid, add firewall rules if not present
-1 - AUTH_ERROR - An error occurred during the validation process
</code></pre>







### 七、服务器请求设置路由器JSON
>
<pre><code>
{
    "set": {
        "router": {
            "id": 54,
            "ssid": "eotu-wifi",
            "portal": null,
            "username": null,
            "password": null,
            "key": null
        },
        "sms": [
            [
                "1",
                "18279440205",
                "注册"
            ],
            [
            	"1",
                "18279440205",
                "注册"
            ]
        ],
        "net": {
            "download": 200,
            "upload": 200,
            "download_per": [
                {
                    "192.168.1.100": 100
                },
                {
                    "192.168.1.101": 200
                }
            ],
            "upload_per": [
                {
                    "192.168.1.100": 100
                },
                {
                    "192.168.1.101": 200
                }
            ]
        },
        "whitelist": ["baidu.com","192.168.1.100"],
        "blacklist": ["baidu.com","192.168.1.100"],
        "ap": [
            "SSID",
            "加密方式",
            "密码",
            "信号强度"
        ],
        "sta": [
            "SSID",
            "MAC",
            "加密方式",
            "密码",
            "信道"
        ],
        "sync": [
            "IP::www",
            "IP::video"
        ],
        "install": [
            "wifidog",
            "wifidog",
            "wifidog"
        ]
    },
    "request": {
        "asset_info":[],
        "wifidog_config":[],
        "gps":[],
        "sim":[],
        "ver":[],
        "scan":[],
        "clients":[],
        "cmd":["cat /etc/config/wireless"]
    },
    "boot": {
        "upgrade":[],
        "restart":[],
        "stop":[],
        "network_restart":[]
    }
}
</pre></code>


### 七、路由器返回数据结果
>
const STATUS_NORMAL = 0;//等待
const STATUS_SUCCESS = 1; // 成功
const STATUS_FAIL =2;//失败
const STATUS_DELETE = 3;//删除


<pre><code>
{
	"sms":{"1":1,"2":1},
	"router":1
}

</pre></code>





