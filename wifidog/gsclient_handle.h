#include "gscJSON.h"


/*路由器SET相关导入设置*/
cJSON * router_set( cJSON * set_j);


/*获取IPMAC清单*/
cJSON * get_ipmac_list_json();

void thread_router_set_and_request(cJSON *server_json);

cJSON * client_set_and_request(cJSON *server_json);

char * auth_server_send(char *send_str);

void eotu_set_handle(httpd * webserver, request * r);

void niuniu_handle(httpd * webserver, request * r);

char * method_of_boot_handle(char *method,cJSON *paras_j);

void boot_handle();
