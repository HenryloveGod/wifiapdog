/***********************
*	support for EOTU
*	此文件包含三个功能
*	1	config_json分解成为config_struct,extra_config_j等
*	2	config_struct与extra_config_j等合并成config_json
*	3	extra_config_j等功能的应用导入
*/

#include <string.h>
#include <stdio.h>
#include <syslog.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h> 
#include <sys/stat.h> 
#include <fcntl.h> 
#include <stdlib.h>


#include "gsconfig_json.h"
#include "gscJSON.h"
#include "debug.h"
#include "conf.h"
#include "fw_iptables.h"
#include "gsbf.h"
#include "safe.h"
#include "gsbasemethod.h"


static cJSON *gsconfig_j;
static cJSON *extra_config_j;
static cJSON *asset_info_j;  
static cJSON *register_j;

static gsconfig_struct_t *gsconfig_struct;

gsconfig_struct_t *config_get_gsconfig_struct()
{
	return gsconfig_struct;
}


cJSON * config_get_gsconfig_j(void)
{
    return gsconfig_j;
}

cJSON * config_get_extra_config_j(void)
{
    return extra_config_j;
}

cJSON * config_get_register_j(void)
{
    return register_j;
}


void destroy_config(void)
{
    if(gsconfig_j)
        cJSON_Delete(gsconfig_j);
}



/******************************************************
*
*   eotu extra functions ~~~~~ 
*
*******************************************************/

/*把接收到的文字转换为标准DROP REJECT ACCEPT LOG ULOG*/

char * target_switch(t_firewall_target target)
{
    char *mode;
    switch (target) {
    case TARGET_DROP:
        mode = safe_strdup("DROP");
        break;
    case TARGET_REJECT:
        mode = safe_strdup("REJECT");
        break;
    case TARGET_ACCEPT:
        mode = safe_strdup("ACCEPT");
        break;
    case TARGET_LOG:
        mode = safe_strdup("LOG");
        break;
    case TARGET_ULOG:
        mode = safe_strdup("ULOG");
        break;
    default:
        mode = safe_strdup("TARGET_DROPACCEPT_ERROR");
        break;        
    }
    
    return mode;

}


/*********************************
*
*	新增功能extra_config_j 数据应用< gsclient_set.c >
*
***********************************/
int extra_config_fw_deploy(cJSON *config_j)
{
	cJSON *set_j;
	set_j = cJSON_GetObjectItem(config_j,"set");

	if( cJSON_GetObjectItem(config_j,"eoturequestpath"))
		gsconfig_struct = config_get_gsconfig_struct();
		gsconfig_struct->eoturequestpath = cJSON_GetObjectItem(config_j,"eoturequestpath")->valuestring;

	//导入设置在firewall.c 的fw_init()中!!

	return 0;
}




/********************************************* 
*	把当前config_struct 转化为json格式,即config_json中的一部分
*************************************************/

cJSON * config_struct_to_config_json()
{

	s_config *config;
	cJSON *res_j;

    config = config_get_config();
	res_j = cJSON_CreateObject();

    cJSON *element_j;
    cJSON_AddItemToObject(res_j,"element",element_j =cJSON_CreateObject());

    set_string_member_to_json(element_j,"gw_id",config->gw_id);
    set_string_member_to_json(element_j,"gw_interface",config->gw_interface);
    set_string_member_to_json(element_j,"gw_address",config->gw_address);
    set_number_member_to_json(element_j,"httpdmaxconn",config->httpdmaxconn);
    set_number_member_to_json(element_j,"checkinterval",config->checkinterval);
    set_number_member_to_json(element_j,"is_checkinterval_active",config->is_checkinterval_active);
    set_number_member_to_json(element_j,"clienttimeout",config->clienttimeout);
    set_number_member_to_json(element_j,"daemon",config->daemon);

    cJSON *auth_server_j;

    cJSON_AddItemToObject(res_j,"auth_server",auth_server_j =cJSON_CreateObject());

    set_string_member_to_json(auth_server_j,"Hostname",config->auth_servers->authserv_hostname);
    set_number_member_to_json(auth_server_j,"SSLAvailable",config->ssl_verify);
    set_string_member_to_json(auth_server_j,"Path",config->auth_servers->authserv_path);
    set_string_member_to_json(auth_server_j,"LoginScriptPathFragment",config->auth_servers->authserv_login_script_path_fragment);
    set_string_member_to_json(auth_server_j,"PortalScriptPathFragment",config->auth_servers->authserv_portal_script_path_fragment);
    //MsgScriptPathFragment ==tell body message at status: AUTH_VALIDATION / AUTH_VALIDATION_FAILED /AUTH_DENIED
    set_string_member_to_json(auth_server_j,"MsgScriptPathFragment",config->auth_servers->authserv_msg_script_path_fragment);
    set_string_member_to_json(auth_server_j,"PingScriptPathFragment",config->auth_servers->authserv_ping_script_path_fragment);
    set_string_member_to_json(auth_server_j,"AuthScriptPathFragment",
                config->auth_servers->authserv_auth_script_path_fragment);
    set_number_member_to_json(auth_server_j,"authserv_http_port",config->auth_servers->authserv_http_port);
    set_number_member_to_json(auth_server_j,"authserv_use_ssl",config->auth_servers->authserv_use_ssl);
    set_string_member_to_json(auth_server_j,"HTTPDUserName",config->httpdusername);
    set_string_member_to_json(auth_server_j,"HTTPDPassword",config->httpdpassword);

	/*****************FirewallRuleSet global set 
	FirewallRule (block|drop|allow|log|ulog) [(tcp|udp|icmp) [port X or port-range X:Y]] [to IP/CIDR]
	
	**************************************************/
    cJSON *global_j,*validating_users_j,*known_users_j,*unknown_users_j,*locked_users_j;

	cJSON_AddItemToObject(res_j,"global",global_j = cJSON_CreateArray());
    cJSON_AddItemToObject(res_j,"validating-users",validating_users_j = cJSON_CreateArray());
    cJSON_AddItemToObject(res_j,"unknown-users",unknown_users_j = cJSON_CreateArray());
    cJSON_AddItemToObject(res_j,"known-users",known_users_j = cJSON_CreateArray());
    cJSON_AddItemToObject(res_j,"locked-users",locked_users_j = cJSON_CreateArray());

    /*
    *黑白名单分类，含ip,mac，域名
    *主要为global， 服务器IP设置也在此链表
    *其他：validating-users; known-users;unknown-users;locked-users
    */
    cJSON *taget_j;
    t_firewall_ruleset *rulesetp;
    t_firewall_rule *tfrp;

    bzero(&rulesetp,sizeof(rulesetp));
    bzero(&tfrp,sizeof(tfrp));
    rulesetp = config->rulesets;

    while(rulesetp){
        if (strcmp(rulesetp->name,"global") == 0)
            taget_j = global_j;
        else if (strcmp(rulesetp->name,"validating-users") == 0)
            taget_j = validating_users_j;
        else if (strcmp(rulesetp->name,"known-users") == 0)
            taget_j = known_users_j;
        else if (strcmp(rulesetp->name,"unknown-users") == 0)
            taget_j = unknown_users_j;
        else if (strcmp(rulesetp->name,"locked-users") == 0)
            taget_j = unknown_users_j;
        else {
            debug(LOG_INFO,"the rule name[%s] is not accept!will break out",rulesetp->name);
            break;
        }
        tfrp = rulesetp->rules;
        /*把链表中的rule 设置到json中*/
        while(tfrp){
            cJSON *tmp_j;
            cJSON_AddItemToArray(taget_j,tmp_j = cJSON_CreateObject());
            set_string_member_to_json(tmp_j,"TARGET_method",target_switch(tfrp->target));
            set_string_member_to_json(tmp_j,"protocol",tfrp->protocol);
            set_string_member_to_json(tmp_j,"port",tfrp->port);
            set_string_member_to_json(tmp_j,"mask",tfrp->mask);
            set_number_member_to_json(tmp_j,"mask_is_ipset",tfrp->mask_is_ipset);
            tfrp = tfrp->next;
        }
        rulesetp = rulesetp->next;
    }

	

    return res_j;
}


/*****************************************************
*	把config_struct和extra_config_j整合到config_json中,即返回的part_config_json
******************************************************/


cJSON * set_struct_and_extra_to_config_json()
{
 
    cJSON *part_config_json;

	part_config_json= config_struct_to_config_json();

    if (!part_config_json){
        debug(LOG_ERR, "convert config_struct to config_json format ERROR");
        part_config_json = cJSON_CreateObject(); 
    }      

	if (extra_config_j)
		cJSON_AddItemToObject(part_config_json,"extra_config",extra_config_j);

	if (asset_info_j )
		cJSON_AddItemToObject(part_config_json,"asset_info",asset_info_j);
		
	if (register_j )
		cJSON_AddItemToObject(part_config_json,"register_info",register_j);

	

	return part_config_json;
}




/*保存当前的config_json到文件中*/

int save_config_json_to_file(cJSON *config_j,char *filepath)
{
	
	char *current_time = print_time();
	
	save_time_to_config_j(config_j,"save_time",current_time);
	
    /*json数据 字符串化*/
    char *filestr = cJSON_Print(config_j);

    /*打开文件，把json字符串 全部写入到文件中，保存在dstfile_path文件中*/
    int fd = open(filepath, O_WRONLY , O_CREAT);

    if (fd == -1) {
        debug(LOG_ERR, "Could not open configuration file '%s', " "exiting...", filepath);
        exit(1);
    }

    int rc =  write(fd,filestr,strlen(filestr));
    if(rc == -1)
    {
        debug(LOG_CRIT, "Failed to write[%d] %s; ERROR : %s", rc, filepath, strerror(errno));
        close(fd);
        exit(1);
    }
    close(fd);

    debug(LOG_INFO, "save_config_struct_to_jsonfile  %s  --OK! ",filepath);
    return 0;
}



/******************************************************************************
* Sets the default config_j parameters and initialises the configuration system 
* eotu 
********************************************************************************/

t_firewall_target return_t_firewall_target(char *target)
{
    //t_firewall_target *tft;
    if(strcmp(target,"DROP")==0)
        return TARGET_DROP;
    else if(strcmp(target,"REJECT")==0)
            return TARGET_REJECT;
    else if(strcmp(target,"ACCEPT")==0)
            return TARGET_ACCEPT;
    else if(strcmp(target,"LOG")==0)
            return TARGET_LOG;
    else if(strcmp(target,"ULOG")==0)
            return TARGET_ULOG;
    else
            return TARGET_ERROR;

}

/*黑白名单: 把json数据中的黑白清单一一复制到struct链表中 */
t_firewall_rule * set_fwrule_to_config_struct(cJSON *fwrule_j)
{
    int i=0;
    /*tfr 作为json 首指针， tmp_P作为中转指针*/
    t_firewall_rule *tfr , *tmp_p ;
    bzero(&tfr,sizeof(tfr));
    bzero(&tfr,sizeof(tmp_p));
    cJSON *tmp_j;
    /*黑白名单清单*/
    while((tmp_j = cJSON_GetArrayItem(fwrule_j, i)) != NULL){
        if (tfr ==NULL )
        {
            tfr = safe_malloc(sizeof(t_firewall_rule));
            set_string_member_to_struct(tmp_j,"port",&tfr->port);
            set_string_member_to_struct(tmp_j,"mask",&tfr->mask);
            tfr->target =return_t_firewall_target( cJSON_GetObjectItem(tmp_j , "TARGET_method")->valuestring);
            //tfr->protocol = cJSON_GetObjectItem(tmp_j , "protocol")->valuestring;
            set_string_member_to_struct(tmp_j,"protocol",&tfr->protocol);
            
            cJSON *ipset_json;
            ipset_json = cJSON_GetObjectItem(tmp_j , "mask_is_ipset");
            if(ipset_json ==NULL)
                tfr->mask_is_ipset = 0;
            else
                tfr->mask_is_ipset = ipset_json->valuedouble;
            tfr->next = NULL;
            
            tmp_p = tfr;
        }
        else{
            tmp_p->next =safe_malloc(sizeof(t_firewall_rule));
            tmp_p = tmp_p->next;
            tmp_p->target =return_t_firewall_target( cJSON_GetObjectItem(tmp_j , "TARGET_method")->valuestring);
            set_string_member_to_struct(tmp_j,"port",&tmp_p->port);
            set_string_member_to_struct(tmp_j,"mask",&tmp_p->mask);
            set_string_member_to_struct(tmp_j,"protocol",&tmp_p->protocol);

            cJSON *ipset_json2;
            ipset_json2 = cJSON_GetObjectItem(tmp_j , "mask_is_ipset");
            if(ipset_json2 ==NULL)
                tmp_p->mask_is_ipset = 0;
            else
                tmp_p->mask_is_ipset = ipset_json2->valuedouble;           
        }
        i++;
    }
    
    
    return tfr;
}



/*把json中的rule 设置到struct中的rule，有三种方式
*combine --> 合并
*cover  --> 覆盖
*exclude -->若自身存在，则排除 
*/
t_firewall_ruleset *set_rule_to_ruleset(t_firewall_ruleset *dst_set,t_firewall_ruleset *src_set,char *method)
{
    int flg = 0;
    t_firewall_ruleset *tmp_d_set;
    tmp_d_set = dst_set;

	s_config *config = config_get_config();


    if (strcmp(method,"cover") ==0){
        
        if(config->rulesets ==NULL){
            config->rulesets  = src_set;
            return config->rulesets;
        }
        
        
        while(tmp_d_set){
            if (strcmp(tmp_d_set->name,src_set->name) ==0){
                flg =1;
                tmp_d_set->rules = src_set->rules;
                return dst_set;
            }
            //没有找到，在补充连接在后面
            if (tmp_d_set->next ==NULL && flg ==0){
                tmp_d_set->next = src_set;
                break;
            }
            else{
                tmp_d_set = tmp_d_set->next;
            }
        }

    }
    else if (strcmp(method,"combine") ==0){
        return dst_set;
    }
    else if (strcmp(method,"exclude") ==0){
        return dst_set;
    }
    else
        return dst_set;

}




/*********************************
*
*	config_json原有部分复制到config_struct
*
***********************************/
int part_config_json_to_config_struct(cJSON *config_j)
{
	s_config *config;

	config = config_get_config();

    cJSON *element_j,*authserver_j;
    cJSON *global_j,*fw_validat_j,*fw_unknown_j,*fw_known_j,*fw_locked_j;
    
    element_j =  cJSON_GetObjectItem(config_j,"element");
    authserver_j = cJSON_GetObjectItem(config_j,"auth_server");
    global_j = cJSON_GetObjectItem(config_j,"global");
    fw_validat_j = cJSON_GetObjectItem(config_j,"validating-users");
    fw_unknown_j = cJSON_GetObjectItem(config_j,"unknown-users");
    fw_known_j = cJSON_GetObjectItem(config_j,"known-users");
    fw_locked_j = cJSON_GetObjectItem(config_j,"locked-users");

    if (element_j){
		
        set_number_member_to_struct(element_j,"daemon", &config->daemon);
        set_string_member_to_struct(element_j,"gw_id", &config->gw_id);
        set_string_member_to_struct(element_j,"gw_interface", &config->gw_interface);
        set_string_member_to_struct(element_j,"gw_address", &config->gw_address);
        set_number_member_to_struct(element_j,"gw_port", &config->gw_port);
        set_number_member_to_struct(element_j,"checkinterval", &config->checkinterval);
        set_number_member_to_struct(element_j,"clienttimeout", &config->clienttimeout);

    }

/* authserver part */
    if (authserver_j)
    {
        if(config->auth_servers ==NULL)
            config->auth_servers =safe_malloc(sizeof(t_auth_serv));
        
        set_string_member_to_struct(authserver_j,"Hostname", &config->auth_servers->authserv_hostname);
        set_number_member_to_struct(authserver_j,"authserv_http_port", &config->auth_servers->authserv_http_port);
        set_string_member_to_struct(authserver_j,"Path",& config->auth_servers->authserv_path);
        set_string_member_to_struct(authserver_j,"LoginScriptPathFragment", 
                                &config->auth_servers->authserv_login_script_path_fragment);
        set_string_member_to_struct(authserver_j,"PortalScriptPathFragment", 
                                &config->auth_servers->authserv_portal_script_path_fragment);
        set_string_member_to_struct(authserver_j,"MsgScriptPathFragment",
                                &config->auth_servers->authserv_msg_script_path_fragment);
        set_string_member_to_struct(authserver_j,"PingScriptPathFragment",
                                &config->auth_servers->authserv_ping_script_path_fragment);
        set_string_member_to_struct(authserver_j,"AuthScriptPathFragment",
                                &config->auth_servers->authserv_auth_script_path_fragment);
        set_number_member_to_struct(authserver_j,"authserv_use_ssl",
                                &config->auth_servers->authserv_use_ssl);
        set_string_member_to_struct(authserver_j,"HTTPDUserName",&config->httpdusername);
        set_string_member_to_struct(authserver_j,"HTTPDPassword",&config->httpdpassword);
        if (config->httpdusername && !config->httpdpassword) {
            debug(LOG_ERR, "HTTPDUserName requires a HTTPDPassword to be set.");
           exit(-1);
        }
    }
/* fw ruleset part ,黑白名单设置*/  

    t_firewall_ruleset *global_ruleset;
    t_firewall_ruleset *validating_ruleset;
    t_firewall_ruleset *unknown_ruleset;
    t_firewall_ruleset *known_ruleset;
    t_firewall_ruleset *locked_ruleset;

    if (global_j){
        global_ruleset = safe_malloc(sizeof(t_firewall_ruleset));
        global_ruleset->name = "global";        
        global_ruleset->rules = set_fwrule_to_config_struct(global_j);
        set_rule_to_ruleset(config->rulesets,global_ruleset,"cover");
    }
    if (fw_validat_j){  
        validating_ruleset = safe_malloc(sizeof(t_firewall_ruleset));
        validating_ruleset->name = "validating-users";        
        validating_ruleset->rules = set_fwrule_to_config_struct(fw_validat_j);
        set_rule_to_ruleset(config->rulesets,validating_ruleset,"cover");
    }
    if (fw_unknown_j){
        unknown_ruleset = safe_malloc(sizeof(t_firewall_ruleset));
        unknown_ruleset->name = "unknown-users";        
        unknown_ruleset->rules = set_fwrule_to_config_struct(fw_unknown_j);
        set_rule_to_ruleset(config->rulesets,unknown_ruleset,"cover");
    }
    if (fw_known_j) { 
        known_ruleset = safe_malloc(sizeof(t_firewall_ruleset));
        known_ruleset->name = "known-users";        
        known_ruleset->rules = set_fwrule_to_config_struct(fw_known_j);
        set_rule_to_ruleset(config->rulesets,known_ruleset,"cover");
    }
    if (fw_locked_j){
        locked_ruleset = safe_malloc(sizeof(t_firewall_ruleset));
        locked_ruleset->name = "locked-users";        
        locked_ruleset->rules = set_fwrule_to_config_struct(fw_locked_j);
        set_rule_to_ruleset(config->rulesets,locked_ruleset,"cover");     
    }

	return 0;
}




/*config_json设置，部分设置到struct中，其他设置到extra_config_j中*/

int config_json_set(cJSON *config_j)
{

	/*检测ip mac 是否与系统匹配*/
	check_gsconfig_with_system();
    	
    /*原有config_struct设置*/
    part_config_json_to_config_struct(config_j);



    /*	asset 资产信息,extra_config,两个都保存到文件中*/
    asset_info_j = cJSON_GetObjectItem(config_j,"asset_info");
    extra_config_j = cJSON_GetObjectItem(config_j,"extra_config");
	
    if (asset_info_j)
        write_str_to_file(cJSON_Print(asset_info_j),DEFAULT_ASSETFILE_JSON);
    if(extra_config_j){
        write_str_to_file(cJSON_Print(extra_config_j),DEFAULT_EXTRAFILE_JSON);
        extra_config_fw_deploy(extra_config_j);
    }
    return 0;
}



/****************************************************************************************************
*	从文件中读取字符串，parse到config_json格式，部分设置到config_struct中，其他设置到extra_config_j中
*****************************************************************************************************/

void config_json_read(char *filename)
{
    char *filestr;

    debug(LOG_INFO, "Reading configuration json file '%s'", filename);
    filestr = readtotalfile(filename);

    cJSON *config_j;
	
    config_j = cJSON_Parse(filestr);
    if (config_j){
		/*部分设置到config_struct中，其他设置到extra_config_j中*/
		gsconfig_j = config_j;
		

		config_json_set(config_j);	
    }else{
		debug(LOG_INFO,"READ CONFIG JSON ERROR [%s]!!!!",filename);
    }
    free(filestr);
}

void gsconfig_struct_init(void)
{
	gsconfig_struct = safe_malloc(sizeof(gsconfig_struct_t));
	
	gsconfig_struct->eoturequestpath = EOTUREQUEST;
	gsconfig_struct->auth_down_qty = 0;

}




