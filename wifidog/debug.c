/* vim: set et ts=4 sts=4 sw=4 : */
/********************************************************************\
 * This program is free software; you can redistribute it and/or    *
 * modify it under the terms of the GNU General Public License as   *
 * published by the Free Software Foundation; either version 2 of   *
 * the License, or (at your option) any later version.              *
 *                                                                  *
 * This program is distributed in the hope that it will be useful,  *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of   *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    *
 * GNU General Public License for more details.                     *
 *                                                                  *
 * You should have received a copy of the GNU General Public License*
 * along with this program; if not, contact:                        *
 *                                                                  *
 * Free Software Foundation           Voice:  +1-617-542-5942       *
 * 59 Temple Place - Suite 330        Fax:    +1-617-542-2652       *
 * Boston, MA  02111-1307,  USA       gnu@gnu.org                   *
 *                                                                  *
\********************************************************************/

/** @file debug.c
    @brief Debug output routines
    @author Copyright (C) 2004 Philippe April <papril777@yahoo.com>
*/

#include <stdio.h>
#include <dirent.h>
#include <errno.h>
#include <syslog.h>
#include <stdarg.h>
#include <time.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <string.h>
#include <stdlib.h>

#include "debug.h"
#include "safe.h"
#include "gsbf.h"

static FILE * prfd;
static FILE * clfd;

static char * prpath;
static char * clpath;

static int eotudebugflg = 0;


file_pn_struct_t *get_file_path(char *logfile)
{
	file_pn_struct_t *pn;
	pn = safe_malloc(sizeof(file_pn_struct_t));

	char *sfile = safe_strdup(logfile);
	char *token="";
	char *direct="/";

	token =strsep(&sfile,"/");
	while(sfile){
		if(strcmp(token,"")!=0)
			safe_asprintf(&direct,"%s%s/",direct,token);
		token = strsep(&sfile,"/");
	}
	pn->path=direct;
	pn->name = token;
	return pn;
}


int create_debug_file_dir(file_pn_struct_t *pn)
{

	//判断文件路径是否存在
	if(access(pn->path,0)!=0  ) {
	      if(mkdir(pn->path, 0755)==-1){
	    	  debug(LOG_ERR," mkdir [%s]fail",pn->path);
	    	  return -1;
	      }
	      debug(LOG_NOTICE,"mkdir log path :%s",pn->path);
	}
	return 0;
}


int delete_extra_debug_file(file_pn_struct_t *pn)
{
	DIR *dirptr = NULL;
	struct dirent *entry;
	int qty;
	char *tmpfile;

	//用ls方法省去排序的计算 ：：））
	char *cmd;
	safe_asprintf(&cmd,"ls -lt %s ",pn->path);
	qty=0;
	FILE *pp = popen(cmd, "r"); //建立管道
    char tmp[1024]; //设置一个合适的长度，以存储每一行输出
    while (fgets(tmp, sizeof(tmp), pp) != NULL) {

    	tmpfile = strstr((char *)tmp,pn->name);
    	if(!tmpfile)
    		continue;
    	tmpfile = str_replace(tmpfile,"\n","");
		qty++;
		if(qty>10){
				safe_asprintf(&tmpfile,"%s%s",pn->path,tmpfile);
				debug(LOG_NOTICE,"Log Clean delete %s\r\n", tmpfile);
				remove(tmpfile);
				free(tmpfile);
				qty--;
		}
    }

return 0;

}

/**********************************************************/

FILE *get_file_fd(char *logfile)
{
	FILE *fd=NULL;
	int newflg=0;

    if(strstr(logfile,"process"))
    	fd = prfd;
    else if(strstr(logfile,"clients"))
    	fd = clfd;
    else
    	return NULL;
	/*******************
	 * 判断文件大小，超过2 000 000,2MB,重新创建
	 * *************************/
    struct stat statbuf;
    int size;
    if(stat(logfile,&statbuf) ==0){
    	size=statbuf.st_size;
    	if(size >20000){
    		char *newname;
    		safe_asprintf(&newname,"%s_%s",logfile,print_time());
    		rename(logfile,newname);
    		newflg =1;
    	}
    }

    //新创建时用到
    if(!fd)
    	newflg=1;

    if(newflg ==1){
    	if(fd>0)
    		fclose(fd);
    	//日志路径，在debug_init中创建
		fd=fopen(logfile,"a+");
		if(fd <0){
			debug(LOG_ERR,"open log [%s] fail,change logpath",logfile);
			fd=fopen("/tmp/process.log","a+");
			if(fd <0)
				return NULL;
		}
    }

    return fd;
}





//日志文件初始化
int debug_file_init(char *processlog,char *clientslog)
{
	int fd;
	debug(LOG_NOTICE,"debug init");
	eotudebugflg=1;
	prpath = processlog;
	clpath = clientslog;

	file_pn_struct_t *pn = get_file_path(processlog);
	file_pn_struct_t *cn = get_file_path(clientslog);

	//判断日志路径是否存在，否则创建
	create_debug_file_dir(pn);
	create_debug_file_dir(cn);

	//检测日志文件数量，过量则删除
	delete_extra_debug_file(pn);
	delete_extra_debug_file(cn);

	//检测当前文件大小，过大重命名，再新建
	get_file_fd(processlog);
	get_file_fd(clientslog);

	return 0;

}

int debug_file_close()
{
	if(prfd)
		fclose(prfd);
	if(clfd)
		fclose(clfd);
	return 0;
}




debugconf_t debugconf = {
    .debuglevel = LOG_INFO,
    .log_stderr = 1,
    .log_syslog = 0,
    .syslog_facility = 0
};

/** @internal
Do not use directly, use the debug macro */
void
_debug(const char *filename, int line, const char *function, int level, const char *format, ...)
{
    char buf[28];
    va_list vlist;
    time_t ts;
    sigset_t block_chld;

    time(&ts);
    
	struct tm *local;
	static char time_mark[80];
	
	ts = time(NULL);  
	local=localtime(&ts);  
	strftime(time_mark,80,"%Y-%m-%d %H:%M:%S",local);
	
	if(eotudebugflg ==1){
		if(level == LOG_NOTICE || level <= LOG_ERR ){
			sigemptyset(&block_chld);
			sigaddset(&block_chld, SIGCHLD);
			sigprocmask(SIG_BLOCK, &block_chld, NULL);
			prfd =get_file_fd(prpath);
			//write to procee.log
			fprintf(prfd, "[%.24s][%s]", ctime_r(&ts, buf),function);
			va_start(vlist, format);
			vfprintf(prfd, format, vlist);
			va_end(vlist);
			fputc('\n', prfd);
			sigprocmask(SIG_UNBLOCK, &block_chld, NULL);
		}

		if(level == LOG_RECORD ){
			sigemptyset(&block_chld);
			sigaddset(&block_chld, SIGCHLD);
			sigprocmask(SIG_BLOCK, &block_chld, NULL);
			clfd =get_file_fd(clpath);
			//write to clients.log
			fprintf(clfd, "[%.24s][%s]", ctime_r(&ts, buf),function);
			va_start(vlist, format);
			vfprintf(clfd, format, vlist);
			va_end(vlist);
			fputc('\n', clfd);
			sigprocmask(SIG_UNBLOCK, &block_chld, NULL);
		}
	}
    if (debugconf.debuglevel >= level) {
        sigemptyset(&block_chld);
        sigaddset(&block_chld, SIGCHLD);
        sigprocmask(SIG_BLOCK, &block_chld, NULL);

        if (level <= LOG_WARNING) {
            //fprintf(stdout, "[%d][%.24s][%u](%s:%d)[function:%s]", level, ctime_r(&ts, buf), getpid(), filename, line,function);
                
            //fprintf(stdout, "[%s](%s:%d)[function:%s]", time_mark, filename, line,function);                
                
            va_start(vlist, format);
            vfprintf(stdout, format, vlist);
            va_end(vlist);
            fputc('\n', stdout);
        } else if (debugconf.log_stderr) {
            fprintf(stdout, "[%d][%.24s][%u](%s:%d)[function:%s]", level, ctime_r(&ts, buf), getpid(), filename, line,function);
            //fprintf(stdout, "[%.24s](%s:%d)[function:%s]", time_mark, filename, line,function);                
               
            va_start(vlist, format);
            vfprintf(stdout, format, vlist);
            va_end(vlist);
            fputc('\n', stdout);
        }

        if (debugconf.log_syslog) {
            openlog("eotu_wifidog", LOG_PID, debugconf.syslog_facility);
            va_start(vlist, format);
            vsyslog(level, format, vlist);
            va_end(vlist);
            closelog();
        }
        
        sigprocmask(SIG_UNBLOCK, &block_chld, NULL);
    }
}
