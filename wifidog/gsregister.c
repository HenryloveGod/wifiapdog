/* vim: set sw=4 ts=4 sts=4 et : */
/********************************************************************\
 * This program is free software; you can redistribute it and/or    *
 * modify it under the terms of the GNU General Public License as   *
 * published by the Free Software Foundation; either version 2 of   *
 * the License, or (at your option) any later version.              *
 *                                                                  *
 * This program is distributed in the hope that it will be useful,  *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of   *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    *
 * GNU General Public License for more details.                     *
 *                                                                  *
 * You should have received a copy of the GNU General Public License*
 * along with this program; if not, contact:                        *
 *                                                                  *
 * Free Software Foundation           Voice:  +1-617-542-5942       *
 * 59 Temple Place - Suite 330        Fax:    +1-617-542-2652       *
 * Boston, MA  02111-1307,  USA       gnu@gnu.org                   *
 *                                                                  *
\********************************************************************/

/* $Id$ */
/** @file ping_thread.c
    @brief Periodically checks in with the central auth server so the auth
    server knows the gateway is still up.  Note that this is NOT how the gateway
    detects that the central server is still up.
    @author Copyright (C) 2004 Alexandre Carmel-Veilleux <acv@miniguru.ca>
*/

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <syslog.h>
#include <signal.h>
#include <errno.h>

#include "../config.h"
#include "safe.h"
#include "common.h"

#include "debug.h"
#include "ping_thread.h"
#include "util.h"
#include "centralserver.h"
#include "firewall.h"
#include "gateway.h"
#include "simple_http.h"

#include "gsconfig.h"
#include "gsbf.h"
#include "gsclient_handle.h"
#include "gsclient_request.h"
#include "gscJSON.h"
#include "gsbasemethod.h"
#include "md5.h"

static int authdown = 0;

char *coin_register(t_auth_serv *auth_server)
{
    cJSON *ping_j,*router_j,*user_j;

    router_struct_t *routerconfig = config_get_gsconfig()->router;

    char *ping_string,*sign;
    char *request;
    
    ping_j = cJSON_CreateObject();
    
    cJSON_AddStringToObject(ping_j,"sign", safe_strdup(get_intervalconfig()->sign));
    cJSON_AddItemToObject(ping_j,"router",cJSON_Duplicate(router_struct_to_j(),1));
    cJSON_AddItemToObject(ping_j,"status",cJSON_Duplicate(get_status_j(),1));
    cJSON_AddItemToObject(ping_j,"user_info",cJSON_Duplicate(get_client_list(NULL),1));

    ping_string = safe_strdup(cJSON_Print(ping_j));
    ping_string = str_replace(ping_string,"\t","");
    ping_string = str_replace(ping_string,"\n","");
    ping_string = str_replace(ping_string," ","");
    cJSON_Delete(ping_j);

    safe_asprintf(&request,
             "POST %s%s HTTP/1.0\r\n"
             "Host: %s\r\n"
             "User-Agent: EOTU/%s\r\n"
             "Content-Type: application/json\r\n"
             "content-length:%d\r\n\r\n"
             "%s\r\n",
             auth_server->authserv_path,auth_server->authserv_reg_script_path_fragment,
             auth_server->authserv_hostname,
             VERSION,strlen(ping_string),
             ping_string);
    
    return request;
}


char *coin_auth_check(t_auth_serv *auth_server)
{
	cJSON *ping_j;
    char *request;
    char *ping_string;

	ping_j = get_ping_key_id_info();
    //cJSON_AddItemToObject(ping_j,"router",cJSON_Duplicate(router_struct_to_j(),1));
    //cJSON_AddItemToObject(ping_j,"status",cJSON_Duplicate(get_status_j(),1));

    ping_string = cJSON_Print(ping_j);
    ping_string = str_replace(ping_string,"\t","");
    ping_string = str_replace(ping_string,"\n","");
    ping_string = str_replace(ping_string," ","");

    safe_asprintf(&request,
             "POST %s%s HTTP/1.0\r\n"
             "Host: %s\r\n"
             "User-Agent: EOTU/%s\r\n"
             "Content-Type: application/json\r\n"
             "content-length:%d\r\n\r\n"
             "%s\r\n",
             "/wifi/","auth/check",
             auth_server->authserv_hostname,
             VERSION,strlen(ping_string),
			 ping_string);
    return request;
}

/*************************************************
 * 注册后 ，服务器将返回数据， 赋值到对应config中
 * */
int save_register_to_config(char *str)
{
    cJSON *server_reg;
    server_reg = cJSON_Parse(str);
    
    if(NULL == server_reg){
    	debug(LOG_ERR,"register string Parse Json ERROR!");
    	return -1;
    }
    cJSON *router_j,*set_j;
    set_j = cJSON_GetObjectItem(server_reg,"set");
    if(NULL == set_j){
    	debug(LOG_ERR,"get set json error");
    	return -1;
    }
    router_j = cJSON_GetObjectItem(set_j,"router");
    if(NULL == router_j){
    	debug(LOG_ERR,"register string Parse Json router part ERROR!");
    	return -1;
    }
    s_config *gsconfig = config_get_gsconfig();
    interval_struct_t *intervalconfig =get_intervalconfig();

    gsconfig->router->id = get_number_from_json_by_member(router_j,"id");
    gsconfig->router->key = get_string_from_json_by_member(router_j,"key");
    //gsconfig->router->updated = get_string_from_json_by_member(router_j,"updated");
    debug(LOG_INFO,"register update time is %d",get_number_from_json_by_member(router_j,"updated"));

    get_intervalconfig()->is_register = 1;

    debug(LOG_NOTICE,"get register : id %d key %s ",gsconfig->router->id , gsconfig->router->key);


    return 0;
}


int get_register(t_auth_serv *auth_server,int sockfd)
{
	char *res,*request;
	request = coin_register(auth_server);
	res = get_http_res(auth_server,sockfd,request);

    char *status_line;
    if (NULL == res) {
        debug(LOG_ERR, "There was a problem pinging the auth server!");
        if (!authdown) {
            fw_set_authdown();
            authdown = 1;
            //config_get_gsconfig_struct()->auth_down_qty = config_get_gsconfig_struct()->auth_down_qty +1;
            return -1;
        }
    } else{
        status_line = strsep(&res,"\r\n");
        if(strstr(status_line,"200") >0 ){
            debug(LOG_INFO, "Auth Server connect, and give set json data");
            char *dataj_str ;
            dataj_str = strstr(res,"\r\n\r\n");
            return save_register_to_config(dataj_str);
       }
    }
    return -1;
}

//注册前重新sock连接
int  register_handle()
{
    int is_signed;
    int sockfd;
    t_auth_serv *auth_server = NULL;
    auth_server = get_auth_server();
    sockfd = connect_auth_server();
    if (sockfd == -1) {
        if (!authdown) {
            fw_set_authdown();
            authdown = 1;
        }
        return -1;
    }
    return get_register(auth_server,sockfd);

}


//is_register 为判断依据
int register_judge()
{
	if(get_intervalconfig()->is_register == 0)
		return register_handle();
	else
		return 0;
}

//注册后 开始 主程序;
void register_loop()
{
    char *is_reg_active ="yes";
    while(1) {
        if(register_judge() == 0)
        	break;
        sleep(60);
    }
}


