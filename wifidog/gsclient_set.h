

#include "gscJSON.h"
#include "gsbf.h"




/**	下载之后检测md5值
	cmd = p->value;
	md5 = p->next->value;
	filename = p->next->next->value;
*/

//基础设置
char *router_base_set(cJSON *p);
//整体下载限速
char * download_limit(cJSON *p);
//整体上传限速
char * upload_limit(cJSON *p);
//单IP下载限速
char * download_limit_per_ip(cJSON *p);
//单IP上传限速
char * upload_limit_per_ip(cJSON *p);
//网站白名单域名URL设置
char * net_whitelist_url_set(cJSON *p);
//网站黑名单域名URL设置
char * net_blacklist_url_set(cJSON *p);

// 发送短信
char * send_message(cJSON *p);

/*上传下载文件 */
char * download_file_thread(cJSON *p);
/* 用rsync同步文件*/
char *rsync_file(cJSON *p);
/*安装软件*/
char * opkg_install_ipk(cJSON *p);
/*更新固件*/
char * sysupgrade_firmware(cJSON *p);
/*wifi 无线 AP模式设置*/
char * wifi_ap_mode_set(cJSON *p);
/*wifi 无线 STA模式设置*/
char * wifi_sta_mode_set(cJSON *p);
/*网络速度设置*/
char *net_set(cJSON *p);

/*system restart **/
char *system_restart(cJSON *p);
/*network restart */
char *network_restart(cJSON *p);
/*eotu restart */
char * wdctl_eotu_restart(cJSON *p);
/*eotu stop */
char *wdctl_eotu_stop(cJSON *p);

//取消单IP上传限速
char * d_upload_limit_per_ip(cJSON *p);
//取消单IP下载限速
char * d_download_limit_per_ip(cJSON *p);



int set_psswd(cJSON *p);


