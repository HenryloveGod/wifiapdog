/*
*	guosheng http handle here
*/

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <unistd.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/un.h> 
#include <sys/socket.h>


#include "httpd.h"

#include "safe.h"
#include "debug.h"
#include "conf.h"
#include "auth.h"
#include "firewall.h"
#include "http.h"
#include "client_list.h"
#include "common.h"
#include "centralserver.h"
#include "util.h"
#include "wd_util.h"


#include "gscJSON.h"
#include "config.h"
#include "gateway.h"

#include "gsbf.h"
#include "gsclient_handle.h"
#include "gsconfig_json.h"


static int connect_to_server(const char *sock_name)
{
    int sock;
    struct sockaddr_un sa_un;

    /* Connect to socket */
    sock = socket(AF_UNIX, SOCK_STREAM, 0);
    if (sock < 0) {
        fprintf(stderr, "wdctl: could not get socket (Error: %s)\n", strerror(errno));
        exit(1);
    }
    memset(&sa_un, 0, sizeof(sa_un));
    sa_un.sun_family = AF_UNIX;
    strncpy(sa_un.sun_path, sock_name, (sizeof(sa_un.sun_path) - 1));

    if (connect(sock, (struct sockaddr *)&sa_un, strlen(sa_un.sun_path) + sizeof(sa_un.sun_family))) {
        fprintf(stderr, "wdctl: wifidog probably not started (Error: %s)\n", strerror(errno));
        exit(1);
    }

    return sock;
}

static size_t send_request(int sock, const char *request)
{
    size_t len;
    ssize_t written;

    len = 0;
    while (len != strlen(request)) {
        written = write(sock, (request + len), strlen(request) - len);
        if (written == -1) {
            fprintf(stderr, "Write to wifidog failed: %s\n", strerror(errno));
            exit(1);
        }
        len += (size_t) written;
    }

    return len;
}

static void send_file_singal(char *filepath)
{
    int sock;
    char buffer[4096];
    char *request;
    ssize_t len;
	s_config * config;
	config = config_get_config();
    sock = connect_to_server(config->wdctl_sock);
	
	safe_asprintf(&request,"file %s\r\n\r\n\r\n",filepath);

    send_request(sock, request);

    while ((len = read(sock, buffer, sizeof(buffer) - 1)) > 0) {
        buffer[len] = '\0';
        fprintf(stdout, "%s", buffer);
    }
	if (strcmp(buffer,"ok")==0){
		debug(LOG_INFO," file start to recieve OK");
	}
    shutdown(sock, 2);
    close(sock);
}





// http请求处理
void gs_http_main(httpd * webserver, request * r)
{
	char *res;	
	char * params = r->readBuf;
	
	params = str_replace(params,r->request.path,"");
	params = str_replace(params," ","");
	params = str_replace(params,"GET","");
	params = str_replace(params,"?","");
	//debug(LOG_INFO,"gshttp_callback --> params:%s \r\n",params);
	cJSON *data_j = cJSON_Parse(params);

	debug(LOG_INFO,"<gs_http_main> path:%s;host:%s;readBuf:%s\r\n",r->request.path,r->request.host ,cJSON_Print(data_j));

	gsconfig_struct_t *gsconfig_struct = config_get_gsconfig_struct();


	if (strcmp(r->request.path,gsconfig_struct->eoturequestpath) == 0){
		cJSON *tmp_config_j;
		tmp_config_j = cJSON_Parse(params);
		if (tmp_config_j){
			cJSON *res_j = client_set_and_request(tmp_config_j);
			res=cJSON_Print(res_j);
		}
	}

    send_http_page(r, "WiFiDog", res);

	free(params);

}



