#!/bin/bash

run(){

local debug="-g -fno-eliminate-unused-debug-types"
local lib=" -lpthread -lm"


local source_file=" gspdu.c gs_sms.c md5.c commandline.c debug.c fw_iptables.c firewall.c gateway.c centralserver.c gsbf.c gscJSON.c gsclient_handle.c gsclient_request.c gsclient_set.c gsconfig.c gsbasemethod.c gsregister.c http.c auth.c client_list.c util.c wdctl_thread.c ping_thread.c safe.c httpd_thread.c simple_http.c pstring.c wd_util.c api.c ip_acl.c protocol.c version.c main.c"

gcc $source_file  $lib $debug -o eotu_wifidog

}


run

echo "gcc OK"
