/*
 * gsconf.c
 *
 *  Created on: 2017年3月9日
 *      Author: dl
 */
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>
#include <pthread.h>
#include <string.h>
#include <ctype.h>
#include <fcntl.h>
#include <errno.h>

#include "gscJSON.h"
#include "gsconfig.h"
#include "safe.h"
#include "debug.h"
#include "gsbf.h"
#include "util.h"
#include "gsbasemethod.h"
#include "gs_sms.h"

cJSON *config_j;
static interval_struct_t *intervalconfig;
static s_config *gsconfig;
static base_struct_t *baseconfig;
static router_struct_t *routerconfig;
static internet_struct_t *internetconfig;
static safe_struct_t *safeconfig;

static int missing_parms;
//static int is_gsconfig_changed;/* 0 no ,1 yes*/
pthread_mutex_t config_mutex = PTHREAD_MUTEX_INITIALIZER;

interval_struct_t *get_intervalconfig()
{
	return intervalconfig;
}
void intervalconfig_init()
{

	intervalconfig = safe_malloc(sizeof(interval_struct_t));
	intervalconfig->auth_down_qty=0;
	intervalconfig->sign="";
	intervalconfig->is_register = 0;
	intervalconfig->is_gsconfig_changed =0;
	intervalconfig->uid ="";
	intervalconfig->token ="";
	intervalconfig->sign_time = 0;
	intervalconfig->is_sms_work = 1; /* 0 disable,1 enable*/
	intervalconfig->bootv =0;

}


s_config *config_get_gsconfig(){
	return gsconfig;
}
base_struct_t *get_baseconfig(){
	return gsconfig->base;
}
router_struct_t *get_routerconfig(){
	return gsconfig->router;
}
internet_struct_t *get_internetconfig(){
	return gsconfig->internet;
}

cJSON *get_config_j()
{
	return config_j;
}


char *get_gw_interface()
{
	char *res;
	res = NULL;
	res = get_popen_str("ifconfig |grep br-lan");
	if(NULL != res)
		return "br-lan";
	return "enp2s0";
}
t_auth_serv *get_auth_server(void)
{
	return gsconfig->auth_servers;
}

/****************************************************
	auth_server  10 个参数
****************************************************/
static void  auth_server_init_with_default_value()
{
	t_auth_serv *auth_server;
	auth_server = safe_malloc(sizeof(t_auth_serv));
	auth_server->authserv_hostname = "eotu.com";
	auth_server->authserv_path = "/wifi/router/";
	auth_server->authserv_login_script_path_fragment = "auth?";
	auth_server->authserv_portal_script_path_fragment = "portal?";
	auth_server->authserv_msg_script_path_fragment = "message?";
	auth_server->authserv_ping_script_path_fragment = "ping?";
	auth_server->authserv_reg_script_path_fragment = "reg?";
	auth_server->authserv_auth_script_path_fragment = "auth?";
	auth_server->authserv_response_script_path_fragment = "response?";
	auth_server->authserv_http_port = 9090;
	auth_server->authserv_ssl_port = 443;
	auth_server->authserv_use_ssl = 0;
	gsconfig->auth_servers = auth_server;
}
void auth_servers_j_to_struct(cJSON *authserver_j)
{
	if(NULL == authserver_j)
		return ;
    t_auth_serv *auth_servers = get_auth_server();
    set_string_member_to_struct(authserver_j,"Hostname", &auth_servers->authserv_hostname);
    set_string_member_to_struct(authserver_j,"Path",&auth_servers->authserv_path);
    set_string_member_to_struct(authserver_j,"LoginScriptPathFragment",&auth_servers->authserv_login_script_path_fragment);
    set_string_member_to_struct(authserver_j,"PortalScriptPathFragment",&auth_servers->authserv_portal_script_path_fragment);
    set_string_member_to_struct(authserver_j,"MsgScriptPathFragment",&auth_servers->authserv_msg_script_path_fragment);
    set_string_member_to_struct(authserver_j,"PingScriptPathFragment",&auth_servers->authserv_ping_script_path_fragment);
    set_string_member_to_struct(authserver_j,"RegScriptPathFragment",&auth_servers->authserv_reg_script_path_fragment);
    set_string_member_to_struct(authserver_j,"ResponseScriptPathFragment",&auth_servers->authserv_response_script_path_fragment);

  set_string_member_to_struct(authserver_j,"AuthScriptPathFragment",&auth_servers->authserv_auth_script_path_fragment);
    set_number_member_to_struct(authserver_j,"authserv_http_port", &auth_servers->authserv_http_port);
    set_number_member_to_struct(authserver_j,"authserv_ssl_port", &auth_servers->authserv_ssl_port);
    set_number_member_to_struct(authserver_j,"authserv_use_ssl",&auth_servers->authserv_use_ssl);
}
cJSON * auth_server_struct_to_j()
{
	t_auth_serv *auth_servers = gsconfig->auth_servers;
	cJSON *auth_server_j;
	auth_server_j =cJSON_CreateObject();

    set_string_member_to_json(auth_server_j,"Hostname",auth_servers->authserv_hostname);
    set_string_member_to_json(auth_server_j,"Path",auth_servers->authserv_path);
    set_string_member_to_json(auth_server_j,"LoginScriptPathFragment",auth_servers->authserv_login_script_path_fragment);
    set_string_member_to_json(auth_server_j,"PortalScriptPathFragment",auth_servers->authserv_portal_script_path_fragment);
    set_string_member_to_json(auth_server_j,"MsgScriptPathFragment",auth_servers->authserv_msg_script_path_fragment);
    set_string_member_to_json(auth_server_j,"PingScriptPathFragment",auth_servers->authserv_ping_script_path_fragment);
    set_string_member_to_json(auth_server_j,"RegScriptPathFragment",auth_servers->authserv_reg_script_path_fragment);
    set_string_member_to_json(auth_server_j,"ResponseScriptPathFragment",auth_servers->authserv_response_script_path_fragment);
    set_string_member_to_json(auth_server_j,"AuthScriptPathFragment",auth_servers->authserv_auth_script_path_fragment);
    set_number_member_to_json(auth_server_j,"authserv_http_port",auth_servers->authserv_http_port);
    set_number_member_to_json(auth_server_j,"authserv_ssl_port",auth_servers->authserv_ssl_port);
    set_number_member_to_json(auth_server_j,"authserv_use_ssl",auth_servers->authserv_use_ssl);
    return auth_server_j;
}
/****************************************************
	base config  9 个参数
****************************************************/
void base_init_with_default_value()
{
	if(NULL == baseconfig)
		baseconfig = safe_malloc(sizeof(base_struct_t));
	baseconfig->configfile = safe_strdup("/etc/wifidog.conf.json");
	baseconfig->htmlmsgfile = safe_strdup("/etc/wifidog-msg.html");
	baseconfig->wdctl_sock = safe_strdup("/tmp/wdctl.sock");
	baseconfig->internal_sock = safe_strdup("/tmp/wdctl.sock");
	baseconfig->deltatraffic = 0;
	baseconfig->daemon = 0;/*0,不开启守护进程*/
    baseconfig->pidfile = NULL;
    baseconfig->arp_table_path = safe_strdup("/proc/net/arp");
    baseconfig->last_change_time=NULL;
    baseconfig->last_use_time = print_time();
    baseconfig->sms_portf = "/dev/ttyUSB3";
    baseconfig->smsc = "8613010720500";
    baseconfig->baudrate = 115200;
    baseconfig->processlog = "/tmp/process.log";
    baseconfig->clientslog = "/tmp/clients.log";

    gsconfig->base=baseconfig;

}
void base_j_to_struct(cJSON *base_j)
{
	if(NULL == baseconfig)
		baseconfig = safe_malloc(sizeof(base_struct_t));
	set_string_member_to_struct(base_j,"configfile", &baseconfig->configfile);
    set_string_member_to_struct(base_j,"htmlmsgfile", &baseconfig->htmlmsgfile);
    set_string_member_to_struct(base_j,"wdctl_sock", &baseconfig->wdctl_sock);
    set_string_member_to_struct(base_j,"internal_sock", &baseconfig->internal_sock);
    set_number_member_to_struct(base_j,"deltatraffic", &baseconfig->deltatraffic);
    set_number_member_to_struct(base_j,"daemon", &baseconfig->daemon);
    set_string_member_to_struct(base_j,"pidfile", &baseconfig->pidfile);
    set_string_member_to_struct(base_j,"arp_table_path", &baseconfig->arp_table_path);
    set_string_member_to_struct(base_j,"last_change_time", &baseconfig->last_change_time);
    set_string_member_to_struct(base_j,"last_use_time", &baseconfig->last_use_time);
    set_string_member_to_struct(base_j,"sms_port", &baseconfig->sms_portf);
    set_string_member_to_struct(base_j,"smsc", &baseconfig->smsc);
    set_number_member_to_struct(base_j,"baudrate", &baseconfig->baudrate);
    set_string_member_to_struct(base_j,"processlog", &baseconfig->processlog);
    set_string_member_to_struct(base_j,"clientslog", &baseconfig->clientslog);
}
cJSON * base_struct_to_j()
{
	baseconfig = gsconfig->base;
	cJSON *base_j;
	if(NULL == (base_j = cJSON_GetObjectItem(config_j,"base")))
		cJSON_AddItemToObject(config_j,"base",base_j =cJSON_CreateObject());
    set_string_member_to_json(base_j,"configfile",baseconfig->configfile);
    set_string_member_to_json(base_j,"htmlmsgfile",baseconfig->htmlmsgfile);
    set_string_member_to_json(base_j,"wdctl_sock",baseconfig->wdctl_sock);
    set_string_member_to_json(base_j,"internal_sock",baseconfig->internal_sock);
    set_number_member_to_json(base_j,"deltatraffic",baseconfig->deltatraffic);
    set_number_member_to_json(base_j,"daemon",baseconfig->daemon);
    set_string_member_to_json(base_j,"pidfile",baseconfig->pidfile);
    set_string_member_to_json(base_j,"arp_table_path",baseconfig->arp_table_path);
    set_string_member_to_json(base_j,"last_change_time",baseconfig->last_change_time);
    set_string_member_to_json(base_j,"last_use_time",baseconfig->last_use_time);
    set_string_member_to_json(base_j,"sms_port",baseconfig->sms_portf);
    set_string_member_to_json(base_j,"smsc",baseconfig->smsc);
    set_string_member_to_json(base_j,"processlog",baseconfig->processlog);
    set_string_member_to_json(base_j,"clientslog",baseconfig->clientslog);
    set_number_member_to_json(base_j,"baudrate",baseconfig->baudrate);
    return base_j;
}

/****************************************************
	internet config  13 个参数
****************************************************/
void internet_init_with_default_value()
{
	if(NULL == internetconfig)
		internetconfig = safe_malloc(sizeof(internet_struct_t));
	internetconfig->external_interface = "enp2s0";
	internetconfig->gw_id = "gwid";
	internetconfig->gw_interface = get_gw_interface();
	internetconfig->gw_address = get_iface_ip(internetconfig->gw_interface);
	internetconfig->gw_port = 2060;
	internetconfig->httpdname = NULL;
	internetconfig->httpdmaxconn = 10;
	internetconfig->httpdrealm = "eotu";
	internetconfig->httpdusername = NULL;
	internetconfig->httpdpassword = NULL;
	internetconfig->clienttimeout = 5;
	internetconfig->checkinterval = 60;
	internetconfig->proxy_port = 0;
	internetconfig->auth_down_max_to_reboot = 10;
	gsconfig->internet=internetconfig;
}
void internet_j_to_struct(cJSON *internet_j)
{
	if(NULL == baseconfig)
		internetconfig = safe_malloc(sizeof(internet_struct_t));
	set_string_member_to_struct(internet_j,"external_interface", &internetconfig->external_interface);
    set_string_member_to_struct(internet_j,"gw_id", &internetconfig->gw_id);
    set_string_member_to_struct(internet_j,"gw_interface", &internetconfig->gw_interface);
    if(internetconfig->gw_address == NULL)
    	set_string_member_to_struct(internet_j,"gw_address", &internetconfig->gw_address);
    set_string_member_to_struct(internet_j,"httpdname", &internetconfig->httpdname);
    set_string_member_to_struct(internet_j,"httpdrealm", &internetconfig->httpdrealm);
    set_string_member_to_struct(internet_j,"httpdusername", &internetconfig->httpdusername);
    set_string_member_to_struct(internet_j,"httpdpassword", &internetconfig->httpdpassword);

    set_number_member_to_struct(internet_j,"gw_port", &internetconfig->gw_port);
    set_number_member_to_struct(internet_j,"httpdmaxconn", &internetconfig->httpdmaxconn);
    set_number_member_to_struct(internet_j,"clienttimeout", &internetconfig->clienttimeout);
    set_number_member_to_struct(internet_j,"checkinterval", &internetconfig->checkinterval);
    set_number_member_to_struct(internet_j,"proxy_port", &internetconfig->proxy_port);
    set_number_member_to_struct(internet_j,"auth_down_max_to_reboot", &internetconfig->auth_down_max_to_reboot);
}
cJSON * internet_struct_to_j()
{
	cJSON *internet_j;
	if(NULL == (internet_j = cJSON_GetObjectItem(config_j,"internet")))
		cJSON_AddItemToObject(config_j,"internet",internet_j =cJSON_CreateObject());
	set_string_member_to_json(internet_j,"external_interface", internetconfig->external_interface);
	set_string_member_to_json(internet_j,"gw_id", internetconfig->gw_id);
	set_string_member_to_json(internet_j,"gw_interface", internetconfig->gw_interface);
	set_string_member_to_json(internet_j,"gw_address", internetconfig->gw_address);
	set_string_member_to_json(internet_j,"httpdname", internetconfig->httpdname);
	set_string_member_to_json(internet_j,"httpdrealm", internetconfig->httpdrealm);
	set_string_member_to_json(internet_j,"httpdusername", internetconfig->httpdusername);
	set_string_member_to_json(internet_j,"httpdpassword", internetconfig->httpdpassword);
    set_number_member_to_json(internet_j,"gw_port", internetconfig->gw_port);
    set_number_member_to_json(internet_j,"httpdmaxconn", internetconfig->httpdmaxconn);
    set_number_member_to_json(internet_j,"clienttimeout", internetconfig->clienttimeout);
    set_number_member_to_json(internet_j,"checkinterval", internetconfig->checkinterval);
    set_number_member_to_json(internet_j,"proxy_port", internetconfig->proxy_port);
    set_number_member_to_json(internet_j,"auth_down_max_to_reboot", internetconfig->auth_down_max_to_reboot);
    return internet_j;
}

/****************************************************
	safe config  9 个参数
****************************************************/
void safe_init_with_default_value()
{
	if(NULL == safeconfig)
		safeconfig = safe_malloc(sizeof(safe_struct_t));

	safeconfig->ssl_certs = safe_strdup("/etc/ssl/certs/");
	safeconfig->ssl_verify = 1;
	safeconfig->ssl_cipher_list = NULL;
	safeconfig->ssl_use_sni = 0;
	gsconfig->safe=safeconfig;
}
void safe_j_to_struct(cJSON *safe_j)
{
	if(NULL == safeconfig)
		safeconfig = safe_malloc(sizeof(safe_struct_t));
	set_string_member_to_struct(safe_j,"HTTPDUserName",&safeconfig->ssl_certs);
    set_string_member_to_struct(safe_j,"HTTPDUserName",&safeconfig->ssl_cipher_list);
    set_number_member_to_struct(safe_j,"HTTPDPassword",&safeconfig->ssl_use_sni);
    set_number_member_to_struct(safe_j,"HTTPDPassword",&safeconfig->ssl_verify);
}
cJSON * safe_struct_to_j()
{
	cJSON *safe_j;
	if(NULL == (safe_j = cJSON_GetObjectItem(config_j,"safe")))
		cJSON_AddItemToObject(config_j,"safe",safe_j =cJSON_CreateObject());
	set_string_member_to_json(safe_j,"HTTPDUserName",safeconfig->ssl_certs);
	set_string_member_to_json(safe_j,"HTTPDUserName",safeconfig->ssl_cipher_list);
    set_number_member_to_json(safe_j,"HTTPDPassword",safeconfig->ssl_use_sni);
    set_number_member_to_json(safe_j,"HTTPDPassword",safeconfig->ssl_verify);
    return safe_j;
}

/****************************************************
	safe config  15 个参数
****************************************************/
void router_init_with_default_value()
{
	if(NULL == routerconfig)
		routerconfig = safe_malloc(sizeof(router_struct_t));
	routerconfig->id=0;
	routerconfig->user_id=NULL;
	routerconfig->ssid=get_popen_str("uci get wireless.stamode.ssid");
	routerconfig->gw_id=internetconfig->gw_id;
	routerconfig->portal="";
	routerconfig->username="root";
	routerconfig->password="";
	routerconfig->mac= get_iface_mac(internetconfig->gw_interface);

	/*serialnumber 在sms_init() 初始化时 IMEI赋值	*/
	routerconfig->ipaddress=internetconfig->gw_address;
	routerconfig->firmware="openwrt cc";
	routerconfig->status=0;
	routerconfig->updated=NULL;
	routerconfig->created=NULL;
	routerconfig->config_ver=NULL;
	gsconfig->router=routerconfig;
}

/*SSID */
int ssid_j_to_struct_cause_boot(cJSON *p)
{
	if(cJSON_GetObjectItem(p,"ssid")){
		if(cJSON_GetObjectItem(p,"ssid")->valuestring){
			char *newssid,*oldssid;
			newssid = cJSON_GetObjectItem(p,"ssid")->valuestring;
			oldssid = config_get_gsconfig()->router->ssid ? config_get_gsconfig()->router->ssid:"none";
			if(strcmp(newssid,oldssid) !=0){
				config_get_gsconfig()->router->ssid = newssid;
				wifi_ap_member_set("ssid",newssid);
				wifi_ap_commit_apply();
				//if(get_intervalconfig()->bootv <2)
				//	get_intervalconfig()->bootv =2;
			}
		}
	}
	return 0;
}

void router_j_to_struct(cJSON *router_j)
{
	if(NULL == routerconfig)
		routerconfig = safe_malloc(sizeof(router_struct_t));
	set_number_member_to_struct(router_j,"id",&routerconfig->id);
    set_string_member_to_struct(router_j,"user_id",&routerconfig->user_id);

	ssid_j_to_struct_cause_boot(router_j);
	set_string_member_to_struct(router_j,"ssid",&routerconfig->ssid);
    set_string_member_to_struct(router_j,"gw_id",&routerconfig->gw_id);
	set_string_member_to_struct(router_j,"portal",&routerconfig->portal);
    set_string_member_to_struct(router_j,"username",&routerconfig->username);
	set_string_member_to_struct(router_j,"password",&routerconfig->password);
    set_string_member_to_struct(router_j,"mac",&routerconfig->mac);
    set_string_member_to_struct(router_j,"serialnumber",&routerconfig->serialnumber);
	set_string_member_to_struct(router_j,"ipaddress",&routerconfig->ipaddress);
    set_string_member_to_struct(router_j,"firmware",&routerconfig->firmware);
    set_number_member_to_struct(router_j,"status",&routerconfig->status);
    set_string_member_to_struct(router_j,"created",&routerconfig->created);
    if(routerconfig->created ==NULL)
    	routerconfig->created = print_time();
    set_string_member_to_struct(router_j,"updated",&routerconfig->updated);
    if(routerconfig->updated ==NULL)
    	routerconfig->updated = routerconfig->created;

    set_string_member_to_struct(router_j,"config_ver",&routerconfig->config_ver);

}

cJSON * get_ping_router_struct_to_j()
{
	cJSON *router_j;
	//if(NULL == (router_j = cJSON_GetObjectItem(config_j,"router")))
		//cJSON_AddItemToObject(router_j,"router",router_j =cJSON_CreateObject());
	router_j =cJSON_CreateObject();
	set_number_member_to_json(router_j,"id",routerconfig->id);
	set_string_member_to_json(router_j,"gw_id",routerconfig->gw_id);
	set_string_member_to_json(router_j,"mac",routerconfig->mac);

	set_string_member_to_json(router_j,"ipaddress",routerconfig->ipaddress);
	set_number_member_to_json(router_j,"status",routerconfig->status);

	if(routerconfig->created ==NULL)
		routerconfig->created =print_time();
	set_string_member_to_json(router_j,"created",routerconfig->created);

	if(routerconfig->updated ==NULL)
		routerconfig->updated =routerconfig->created;
	set_string_member_to_json(router_j,"updated",routerconfig->updated);
	set_string_member_to_json(router_j,"config_ver",routerconfig->config_ver);
	return router_j;
}



cJSON * router_struct_to_j()
{
	cJSON *router_j;
	if(NULL == (router_j = cJSON_GetObjectItem(config_j,"router")))
		cJSON_AddItemToObject(router_j,"router",router_j =cJSON_CreateObject());
	set_number_member_to_json(router_j,"id",routerconfig->id);
	set_string_member_to_json(router_j,"user_id",routerconfig->user_id);
	set_string_member_to_json(router_j,"ssid",routerconfig->ssid);
	set_string_member_to_json(router_j,"gw_id",routerconfig->gw_id);
	set_string_member_to_json(router_j,"portal",routerconfig->portal);
	set_string_member_to_json(router_j,"username",routerconfig->username);
	//imei 转移到set_imei()处理了
	set_string_member_to_json(router_j,"password",routerconfig->password);
	set_string_member_to_json(router_j,"mac",routerconfig->mac);
	set_string_member_to_json(router_j,"ipaddress",routerconfig->ipaddress);
	set_string_member_to_json(router_j,"firmware",routerconfig->firmware);
	set_number_member_to_json(router_j,"status",routerconfig->status);
	if(routerconfig->created ==NULL)
		routerconfig->created =print_time();
	set_string_member_to_json(router_j,"created",routerconfig->created);

	if(routerconfig->updated ==NULL)
		routerconfig->updated =routerconfig->created;
	set_string_member_to_json(router_j,"updated",routerconfig->updated);
	set_string_member_to_json(router_j,"config_ver",routerconfig->config_ver);
	return router_j;
}






/******************************************************************************
* Sets the default config_j parameters and initialises the configuration system
* eotu
********************************************************************************/
t_firewall_target return_t_firewall_target(char *target)
{
    //t_firewall_target *tft;
    if(strcmp(target,"DROP")==0)
        return TARGET_DROP;
    else if(strcmp(target,"REJECT")==0)
            return TARGET_REJECT;
    else if(strcmp(target,"ACCEPT")==0)
            return TARGET_ACCEPT;
    else if(strcmp(target,"LOG")==0)
            return TARGET_LOG;
    else if(strcmp(target,"ULOG")==0)
            return TARGET_ULOG;
    else
            return TARGET_ERROR;

}
/*黑白名单: 把json数据中的黑白清单一一复制到struct链表中 */
t_firewall_rule * fwrule_j_to_struct(cJSON *fwrule_j)
{
    int i=0;
    /*tfr 作为json 首指针， tmp_P作为中转指针*/
    t_firewall_rule *tfr , *tmp_p ;
    bzero(&tfr,sizeof(tfr));
    bzero(&tfr,sizeof(tmp_p));
    cJSON *tmp_j;
    /*黑白名单清单*/
    while((tmp_j = cJSON_GetArrayItem(fwrule_j, i)) != NULL){
        if (tfr ==NULL )
        {
            tfr = safe_malloc(sizeof(t_firewall_rule));
            set_string_member_to_struct(tmp_j,"port",&tfr->port);
            set_string_member_to_struct(tmp_j,"mask",&tfr->mask);
            tfr->target =return_t_firewall_target( cJSON_GetObjectItem(tmp_j , "TARGET_method")->valuestring);
            //tfr->protocol = cJSON_GetObjectItem(tmp_j , "protocol")->valuestring;
            set_string_member_to_struct(tmp_j,"protocol",&tfr->protocol);
            cJSON *ipset_json;
            ipset_json = cJSON_GetObjectItem(tmp_j , "mask_is_ipset");
            if(ipset_json ==NULL)
                tfr->mask_is_ipset = 0;
            else
                tfr->mask_is_ipset = ipset_json->valuedouble;
            tfr->next = NULL;
            tmp_p = tfr;
        }
        else{
            tmp_p->next =safe_malloc(sizeof(t_firewall_rule));
            tmp_p = tmp_p->next;
            tmp_p->target =return_t_firewall_target( cJSON_GetObjectItem(tmp_j , "TARGET_method")->valuestring);
            set_string_member_to_struct(tmp_j,"port",&tmp_p->port);
            set_string_member_to_struct(tmp_j,"mask",&tmp_p->mask);
            set_string_member_to_struct(tmp_j,"protocol",&tmp_p->protocol);
            cJSON *ipset_json2;
            ipset_json2 = cJSON_GetObjectItem(tmp_j , "mask_is_ipset");
            if(ipset_json2 ==NULL)
                tmp_p->mask_is_ipset = 0;
            else
                tmp_p->mask_is_ipset = ipset_json2->valuedouble;
        }
        i++;
    }
    return tfr;
}

/*********************************************************
 * 把json中的rule 设置到struct中的rule，有三种方式
*combine --> 合并
*cover  --> 覆盖
*exclude -->若自身存在，则排除
***********************************************************/
t_firewall_ruleset *set_rule_to_ruleset(t_firewall_ruleset *dst_set,t_firewall_ruleset *src_set,char *method)
{
    int flg = 0;
    t_firewall_ruleset *tmp_d_set;
    tmp_d_set = dst_set;

    if (strcmp(method,"cover") ==0){
        if(gsconfig->rulesets ==NULL){
        	gsconfig->rulesets  = src_set;
            return gsconfig->rulesets;
        }

        while(tmp_d_set){
            if (strcmp(tmp_d_set->name,src_set->name) ==0){
                flg =1;
                tmp_d_set->rules = src_set->rules;
                return dst_set;
            }
            if (tmp_d_set->next ==NULL && flg ==0){//没有找到，在补充连接在后面
                tmp_d_set->next = src_set;
                break;
            }
            else{
                tmp_d_set = tmp_d_set->next;
            }
        }
    }
    else if (strcmp(method,"combine") ==0)
    	//....on going
        return dst_set;
    else if (strcmp(method,"exclude") ==0)
    	//....on going
        return dst_set;
    else
    	//....on going
        return dst_set;
    return NULL;
}

void fw_j_to_struct(cJSON *fw_j,char *fw_name,char *cover)
{
	t_firewall_ruleset *fw;
	fw = safe_malloc(sizeof(t_firewall_ruleset));
	fw->name = fw_name;
	fw->rules = fwrule_j_to_struct(fw_j);
    set_rule_to_ruleset(gsconfig->rulesets,fw,"cover");
}

/*****************FirewallRuleSet global set
FirewallRule (block|drop|allow|log|ulog) [(tcp|udp|icmp) [port X or port-range X:Y]] [to IP/CIDR]
**************************************************/

/*把接收到的文字转换为标准DROP REJECT ACCEPT LOG ULOG*/
char * target_switch(t_firewall_target target)
{
    char *mode;
    switch (target) {
    case TARGET_DROP:
        mode = safe_strdup("DROP");
        break;
    case TARGET_REJECT:
        mode = safe_strdup("REJECT");
        break;
    case TARGET_ACCEPT:
        mode = safe_strdup("ACCEPT");
        break;
    case TARGET_LOG:
        mode = safe_strdup("LOG");
        break;
    case TARGET_ULOG:
        mode = safe_strdup("ULOG");
        break;
    default:
        mode = safe_strdup("TARGET_DROPACCEPT_ERROR");
        break;
    }
    return mode;
}
void fw_struct_to_j()
{
    cJSON *global_j,*validating_users_j,*known_users_j,*unknown_users_j,*locked_users_j;
    if(NULL == (global_j = cJSON_GetObjectItem(config_j,"global")))
    	cJSON_AddItemToObject(config_j,"global",global_j = cJSON_CreateArray());
    if(NULL == (validating_users_j = cJSON_GetObjectItem(config_j,"validating-users")))
    	cJSON_AddItemToObject(config_j,"validating-users",validating_users_j = cJSON_CreateArray());
    if(NULL == (unknown_users_j = cJSON_GetObjectItem(config_j,"unknown-users")))
    	cJSON_AddItemToObject(config_j,"unknown-users",unknown_users_j = cJSON_CreateArray());
    if(NULL == (known_users_j = cJSON_GetObjectItem(config_j,"known-users")))
    	cJSON_AddItemToObject(config_j,"known-users",known_users_j = cJSON_CreateArray());
    if(NULL == (locked_users_j = cJSON_GetObjectItem(config_j,"locked-users")))
    	cJSON_AddItemToObject(config_j,"locked-users",locked_users_j = cJSON_CreateArray());

    /*
    *黑白名单分类，含ip,mac，域名
    *主要为global， 服务器IP设置也在此链表
    *其他：validating-users; known-users;unknown-users;locked-users
    */
    cJSON *taget_j;
    t_firewall_ruleset *rulesetp;
    t_firewall_rule *tfrp;
    bzero(&rulesetp,sizeof(rulesetp));
    bzero(&tfrp,sizeof(tfrp));
    rulesetp = gsconfig->rulesets;
    while(rulesetp){
        if (strcmp(rulesetp->name,"global") == 0)
            taget_j = global_j;
        else if (strcmp(rulesetp->name,"validating-users") == 0)
            taget_j = validating_users_j;
        else if (strcmp(rulesetp->name,"known-users") == 0)
            taget_j = known_users_j;
        else if (strcmp(rulesetp->name,"unknown-users") == 0)
            taget_j = unknown_users_j;
        else if (strcmp(rulesetp->name,"locked-users") == 0)
            taget_j = unknown_users_j;
        else {
            debug(LOG_ERR,"the rule name[%s] is not accept ERROR!will break out",rulesetp->name);
            break;
        }
        tfrp = rulesetp->rules;
        /*把链表中的rule 设置到json中*/
        while(tfrp){
            cJSON *tmp_j;
            cJSON_AddItemToArray(taget_j,tmp_j = cJSON_CreateObject());
            set_string_member_to_json(tmp_j,"TARGET_method",target_switch(tfrp->target));
            set_string_member_to_json(tmp_j,"protocol",tfrp->protocol);
            set_string_member_to_json(tmp_j,"port",tfrp->port);
            set_string_member_to_json(tmp_j,"mask",tfrp->mask);
            set_number_member_to_json(tmp_j,"mask_is_ipset",tfrp->mask_is_ipset);
            tfrp = tfrp->next;
        }
        rulesetp = rulesetp->next;
    }
}

/***************************************
 *  debug_init 初始化
 * **************************************/
void debug_init()
{
    /*LOG SET*/
    debugconf.log_stderr = 1;
    debugconf.debuglevel = 0;
    debugconf.syslog_facility = LOG_DAEMON;
    debugconf.log_syslog = 0;
}


/*********************whitelist to json*/

void whitelist_j_to_struct(cJSON *p)
{
	if(NULL ==  p)
		return ;
	if(NULL == gsconfig->whitelist)
		gsconfig->whitelist = safe_malloc(sizeof(whitelist_struct_t));

	whitelist_struct_t *wt,*tmp_wt;
	wt = gsconfig->whitelist;
	tmp_wt =NULL;
	cJSON *tmp_j=NULL;

	int i=0;
	while((tmp_j = cJSON_GetArrayItem(p, i)) != NULL){
		if(NULL == tmp_j->valuestring)
			break;
		if(NULL == wt)
			wt = safe_malloc(sizeof(whitelist_struct_t));
		if(tmp_wt)
			tmp_wt->next = wt;
		wt->url = tmp_j->valuestring;
		if(NULL == gsconfig->whitelist)
			gsconfig->whitelist =wt;
		tmp_wt =wt;
		wt= wt->next;
		i++;
	}
}

cJSON *whitelist_struct_to_j()
{
	whitelist_struct_t *tmp_wt;
	tmp_wt = gsconfig->whitelist;

	cJSON *p=NULL;
	if(NULL == (p = cJSON_GetObjectItem(config_j,"whitelist")))
		cJSON_AddItemToObject(config_j,"whitelist",p =cJSON_CreateObject());

	cJSON *cp=cJSON_CreateObject();

	cp = p->child ;

	while(tmp_wt){
		if(NULL == tmp_wt->url)
			break;
		if(NULL == cp)
			cp = cJSON_CreateObject();
		cp->valuestring = tmp_wt->url;
		cp =cp->next;
		tmp_wt = tmp_wt->next;
	}
	return p;
}

/*********************blacklist to json*/

void blacklist_j_to_struct(cJSON *p)
{
	if(NULL ==  p)
		return ;
	if(NULL == gsconfig->blacklist)
		gsconfig->blacklist = safe_malloc(sizeof(blacklist_struct_t));

	blacklist_struct_t *wt,*tmp_wt;
	wt = gsconfig->blacklist;
	tmp_wt =NULL;
	cJSON *tmp_j=NULL;

	int i=0;
	while((tmp_j = cJSON_GetArrayItem(p, i)) != NULL){
		if(NULL == tmp_j->valuestring)
			break;
		if(NULL == wt)
			wt = safe_malloc(sizeof(blacklist_struct_t));
		if(tmp_wt)
			tmp_wt->next = wt;
		wt->url = tmp_j->valuestring;
		if(NULL == gsconfig->blacklist)
			gsconfig->blacklist =wt;
		tmp_wt =wt;
		wt= wt->next;
		i++;
	}
}

cJSON *blacklist_struct_to_j()
{
	blacklist_struct_t *tmp_wt;
	tmp_wt = gsconfig->blacklist;

	cJSON *p=NULL;
	if(NULL == (p = cJSON_GetObjectItem(config_j,"blacklist")))
		cJSON_AddItemToObject(config_j,"blacklist",p =cJSON_CreateObject());

	cJSON *cp=cJSON_CreateObject();

	cp = p->child ;

	while(tmp_wt){
		if(NULL == tmp_wt->url)
			break;
		if(NULL == cp)
			cp = cJSON_CreateObject();
		cp->valuestring = tmp_wt->url;
		cp =cp->next;
		tmp_wt = tmp_wt->next;
	}
	return p;
}

/***************************************
 *  gsconfig 初始化
 * **************************************/
void gsconfig_init_with_default_value()
{
    debug(LOG_DEBUG, "Setting default config parameters");

    gsconfig = safe_malloc(sizeof(s_config));

    /*auth-server info*/
    auth_server_init_with_default_value();
    base_init_with_default_value();
    internet_init_with_default_value();
    safe_init_with_default_value();
    router_init_with_default_value();
    /*other rule set*/
    gsconfig->rulesets = NULL;
    gsconfig->trustedmaclist = NULL;
    gsconfig->popular_servers = NULL;

    gsconfig->whitelist =NULL ;
    gsconfig->blacklist =NULL;

    debug_init();
}

/*把json配置 赋值到gsconfig中*/
void gsconfig_j_to_struct(cJSON *config_j)
{

	debug(LOG_NOTICE, "check config if new comparing with wifidog.conf.json_file ");

    cJSON *router_j,*internet_j,*base_j,*authserver_j,*safe_j;
    cJSON *global_j,*fw_validat_j,*fw_unknown_j,*fw_known_j,*fw_locked_j;
    cJSON *whitelist_j,*blacklist_j;
    router_j =  cJSON_GetObjectItem(config_j,"router");
    internet_j =  cJSON_GetObjectItem(config_j,"internet");
    base_j =  cJSON_GetObjectItem(config_j,"base");
    authserver_j = cJSON_GetObjectItem(config_j,"auth_server");
    safe_j = cJSON_GetObjectItem(config_j,"safe");
    global_j = cJSON_GetObjectItem(config_j,"global");
    fw_validat_j = cJSON_GetObjectItem(config_j,"validating-users");
    fw_unknown_j = cJSON_GetObjectItem(config_j,"unknown-users");
    fw_known_j = cJSON_GetObjectItem(config_j,"known-users");
    fw_locked_j = cJSON_GetObjectItem(config_j,"locked-users");

    whitelist_j = cJSON_GetObjectItem(config_j,"whitelist");
    blacklist_j = cJSON_GetObjectItem(config_j,"blacklist");

    if (router_j)
    	router_j_to_struct(router_j);
    if (internet_j)
    	internet_j_to_struct(internet_j);
    if (base_j)
    	base_j_to_struct(base_j);
    if (authserver_j)
    	auth_servers_j_to_struct(authserver_j);
    if(safe_j)
    	safe_j_to_struct(safe_j);

    if (whitelist_j)
    	whitelist_j_to_struct(whitelist_j);
    if(blacklist_j)
    	blacklist_j_to_struct(blacklist_j);




/* fw ruleset part ,黑白名单设置
    if (global_j)
    	fw_j_to_struct(global_j,"global","cover");
    if (fw_validat_j)
    	fw_j_to_struct(fw_validat_j,"validating-users","cover");
    if (fw_unknown_j)
    	fw_j_to_struct(fw_unknown_j,"unknown-users","cover");
    if (fw_known_j)
    	fw_j_to_struct(fw_unknown_j,"known-users","cover");
    if (fw_locked_j)
    	fw_j_to_struct(fw_unknown_j,"locked-users","cover");
*/
}

void gsconfig_struct_to_j()
{
	if( NULL == config_j)
		config_j = cJSON_CreateObject();
	base_struct_to_j();
	internet_struct_to_j();
	safe_struct_to_j();
	auth_server_struct_to_j();
	//fw_struct_to_j();

	router_struct_to_j();

	whitelist_struct_to_j();
	blacklist_struct_to_j();

}



/*
 * 获取IMEI，赋值给serialnumber
 * serialnumber 为空，则以时间为准
 * 通过这个命令吧：	smsport /dev/ttyUSB3 AT+CGSN
 * */

int set_imei()
{
	char *cmd;
	int isgetimei=0;
	safe_asprintf(&cmd,"smsport %s AT+CGSN",gsconfig->base->sms_portf);

	char *imeir =get_popen_str(cmd);

	if(!imeir){
		debug(LOG_ERR,"smsport run fail!");
		return -1;
	}

	char *token = strstr(imeir,"smsresult");
	char imei[16];
	int i=0;

	while(token){
		if( *token >= 0+'0' && *token <= 9+'0' ){
			imei[i]=*token;
			i++;
			if(i>14){
				break;
			}
		}
		token++;
	}
	imei[i]='\0';

	if(i==15){
		if(NULL == routerconfig->serialnumber || strcmp(routerconfig->serialnumber,imei) !=0){
			debug(LOG_NOTICE,"serialnumber change to : %s" ,imei);
			routerconfig->serialnumber =safe_strdup(imei);
			set_string_member_to_json(cJSON_GetObjectItem(config_j,"router"),"serialnumber",routerconfig->serialnumber);
		}
	}

	return 0;

}


int gsconfig_read()
{
	/*初始化值*/

	intervalconfig_init();
	gsconfig_init_with_default_value();
	config_j = cJSON_Parse(read_file_to_string(baseconfig->configfile));

	if(config_j){
		gsconfig_j_to_struct(config_j);
	}else
		debug(LOG_ERR,"cJSON_Parse config error! [%s]",baseconfig->configfile);

	return 0;

}

/** @internal
    Verifies that a required parameter is not a null pointer
*/
static void config_notnull(const void *parm, const char *parmname)
{
    if (parm == NULL) {
        debug(LOG_ERR, "%s is not set", parmname);
        missing_parms = 1;
    }
}

/** Verifies if the configuration is complete and valid.  Terminates the program if it isn't */
void gsconfig_validate(void)
{
    config_notnull(internetconfig->gw_interface, "GatewayInterface");
    config_notnull(gsconfig->auth_servers, "AuthServer");
    validate_popular_servers();

    if (missing_parms) {
        debug(LOG_ERR, "Configuration is not complete, exiting...");
        exit(-1);
    }
}


/**
 * This function marks the current auth_server, if it matches the argument,
 * as bad. Basically, the "bad" server becomes the last one on the list.
 */
void
mark_auth_server_bad(t_auth_serv * bad_server)
{
    t_auth_serv *tmp;
    if (gsconfig->auth_servers == bad_server && bad_server->next != NULL) {
        /* Go to the last */
        for (tmp = gsconfig->auth_servers; tmp->next != NULL; tmp = tmp->next) ;
        /* Set bad server as last */
        tmp->next = bad_server;
        /* Remove bad server from start of list */
        gsconfig->auth_servers = bad_server->next;
        /* Set the next pointe to NULL in the last element */
        bad_server->next = NULL;
    }

}


/** @internal
 * Add a popular server to the list. It prepends for simplicity.
 * @param server The hostname to add.
 */
static void
add_popular_server(const char *server)
{
    t_popular_server *p = NULL;

    p = (t_popular_server *)safe_malloc(sizeof(t_popular_server));
    p->hostname = safe_strdup(server);

    if (gsconfig->popular_servers == NULL) {
        p->next = NULL;
        gsconfig->popular_servers = p;
    } else {
        p->next = gsconfig->popular_servers;
        gsconfig->popular_servers = p;
    }
}

/** @internal
 * Validate that popular servers are populated or log a warning and set a default.
 */
static void
validate_popular_servers(void)
{
    if (gsconfig->popular_servers == NULL) {
        debug(LOG_WARNING, "PopularServers not set in config file, this will become fatal in a future version.");
        add_popular_server("www.eotu.com");
    }
}

t_firewall_rule * get_ruleset(const char *ruleset)
{
    t_firewall_ruleset *tmp;
    for (tmp = gsconfig->rulesets; tmp != NULL && strcmp(tmp->name, ruleset) != 0; tmp = tmp->next) ;
    if (tmp == NULL)
        return NULL;
    return (tmp->rules);
}

/**
 * Parse possiblemac to see if it is valid MAC address format */
int check_mac_format(char *possiblemac)
{
    char hex2[3];
    return
        sscanf(possiblemac,"%2[A-Fa-f0-9]:%2[A-Fa-f0-9]:%2[A-Fa-f0-9]:%2[A-Fa-f0-9]:%2[A-Fa-f0-9]:%2[A-Fa-f0-9]",
               hex2, hex2, hex2, hex2, hex2, hex2) == 6;
}



/*保存当前的config_json到文件中*/

int save_config_json_to_file(cJSON *config_j,char *filepath)
{

    /*json数据 字符串化*/
    char *filestr = cJSON_Print(config_j);
    /*打开文件，把json字符串 全部写入到文件中，保存在dstfile_path文件中*/
    int fd = open(filepath, O_WRONLY , O_CREAT,O_TRUNC);
    if (fd == -1) {
        debug(LOG_ERR, "Could not open configuration file '%s', " "exiting...", filepath);
        exit(1);
    }

    int rc =  write(fd,filestr,strlen(filestr));
    if(rc == -1 ) {
        debug(LOG_CRIT, "Failed to write[%d] %s; ERROR : %s", rc, filepath, strerror(errno));
        close(fd);
        exit(1);
    }
    close(fd);

    debug(LOG_NOTICE, "success to save config to file  %s ! ",filepath);

    return 0;
}
