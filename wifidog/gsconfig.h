/* vim: set et sw=4 ts=4 sts=4 : */
/* $Id$ */
/** @file conf.h
    @brief Config file parsing
    @author Copyright (C) 2004 Philippe April <papril777@yahoo.com>
*/

/*
 *
 * LOG_NOTICE	储存重要过程的日志	process.log
 * LOG_ERROR	储存重要错误的日志	process.log
 * LOG_RECORD	储存用户相关的日志	clients.log
 * */




#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "gscJSON.h"

#ifndef _CONFIG_H_
#define _CONFIG_H_

typedef struct interval_struct{
	int auth_down_qty;
	char *sign;
	int is_register; /* 0 no ; 1 yes*/
	int is_gsconfig_changed;/* 0 no ; 1 yes*/
	char *uid;
	char *token;
	long  sign_time;
	int is_sms_work;
	int bootv;//1 eotu restart ,2 network restart , 3 system restart;0 none
}interval_struct_t;


/*@{*/
/** Defines for firewall rule sets. */
#define FWRULESET_GLOBAL "global"
#define FWRULESET_VALIDATING_USERS "validating-users"
#define FWRULESET_KNOWN_USERS "known-users"
#define FWRULESET_AUTH_IS_DOWN "auth-is-down"
#define FWRULESET_UNKNOWN_USERS "unknown-users"
#define FWRULESET_LOCKED_USERS "locked-users"
#define DEFAULT_SERVICE_CENTER_NUMBER "8613010720500"
/*@}*/
/**
 * Mutex for the configuration file, used by the auth_servers related
 * functions. */

extern pthread_mutex_t config_mutex;

/**
 * Information about the authentication server
 */
typedef struct _auth_serv_t {
    char *authserv_hostname;    /**< @brief Hostname of the central server */
    char *authserv_path;        /**< @brief Path where wifidog resides */
    char *authserv_login_script_path_fragment;  /**< @brief This is the script the user will be sent to for login. */
    char *authserv_portal_script_path_fragment; /**< @brief This is the script the user will be sent to after a successfull login. */
    char *authserv_msg_script_path_fragment;    /**< @brief This is the script the user will be sent to upon error to read a readable message. */
    char *authserv_ping_script_path_fragment;   /**< @brief This is the ping heartbeating script. */
    char *authserv_auth_script_path_fragment;   /**< @brief This is the script that talks the wifidog gateway protocol. */
    char *authserv_reg_script_path_fragment;   /**< @brief This is the script that talks the wifidog gateway protocol. */
    char *authserv_response_script_path_fragment;   /**< @brief This is the script that talks the wifidog gateway protocol. */

    int authserv_http_port;     /**< @brief Http port the central server
				     listens on */
    int authserv_ssl_port;      /**< @brief Https port the central server
				     listens on */
    int authserv_use_ssl;       /**< @brief Use SSL or not */
    char *last_ip;      /**< @brief Last ip used by authserver */
    struct _auth_serv_t *next;
} t_auth_serv;

typedef struct base_struct{
    char *configfile;       /**< @brief name of the config file */
    char *htmlmsgfile;          /**< @brief name of the HTML file used for messages */
    char *wdctl_sock;           /**< @brief wdctl path to socket */
    char *internal_sock;                /**< @brief internal path to socket */
    int deltatraffic;                   /**< @brief reset each user's traffic (Outgoing and Incoming) value after each Auth operation. */
    int daemon;                 /**< @brief if daemon > 0, use daemon mode */
    char *pidfile;            /**< @brief pid file path of wifidog */
    char *arp_table_path; /**< @brief Path to custom ARP table, formattedlike /proc/net/arp */
	char *last_use_time;//local sw launch time
	char *last_change_time;//local sw config change time
	char *sms_portf; //default /dev/ttyUSB3
	char *smsc; // 短信中心号码
	int baudrate;
	char *processlog;
	char *clientslog;

}base_struct_t;

typedef struct router_struct{
	int id;
	char *user_id;
	char *ssid;
	char *gw_id;
	char *serialnumber;
	char *portal;
	char *username;
	char *password;
	char *key;
	char *mac;
	char *ipaddress;
	char *firmware;
	int status;  //0 normal , 1 error , 2 logout
	char *updated; // add update time if server register new one
	char *created; // 本程序第一次使用时
	char *config_ver;

}router_struct_t;
typedef struct internet_struct{
	char *external_interface;   /**< @brief External network interface name for firewall rules */
	char *gw_id;                /**< @brief ID of the Gateway, sent to central server */
	char *gw_interface;         /**< @brief Interface we will accept connections on */
	char *gw_address;           /**< @brief Internal IP address for our web server */
	int gw_port;                /**< @brief Port the webserver will run on */
	char *httpdname;            /**< @brief Name the web server will return when  replying to a request */
	int httpdmaxconn;           /**< @brief Used by libhttpd, not sure what it  does */
	char *httpdrealm;           /**< @brief HTTP Authentication realm */
	char *httpdusername;        /**< @brief Username for HTTP authentication */
	char *httpdpassword;        /**< @brief Password for HTTP authentication */
	int clienttimeout;          /**< @brief How many CheckIntervals before a client   must be re-authenticated */
	int checkinterval;          /**< @brief Frequency the the client timeout check   thread will run. */
    int proxy_port;             /**< @brief Transparent proxy port (0 to disable) */
	int auth_down_max_to_reboot;	/*超过次数后重启*/
}internet_struct_t;

typedef struct safe_struct{
    char *ssl_certs;            /**< @brief Path to SSL certs for auth server	verification */
    int ssl_verify;             /**< @brief boolean, whether to enable	auth server certificate verification */
    char *ssl_cipher_list;  /**< @brief List of SSL ciphers allowed. Optional. */
    int ssl_use_sni;            /**< @brief boolean, whether to enable */
}safe_struct_t;



/**
 * Firewall targets
 */
typedef enum {
    TARGET_DROP,
    TARGET_REJECT,
    TARGET_ACCEPT,
    TARGET_LOG,
    TARGET_ULOG,
    TARGET_ERROR
} t_firewall_target;

/**
 * Firewall rules and ruleset
 */
typedef struct _firewall_rule_t {
    t_firewall_target target;   /**< @brief t_firewall_target */
    char *protocol;             /**< @brief tcp, udp, etc ... */
    char *port;                 /**< @brief Port to block/allow */
    char *mask;                 /**< @brief Mask for the rule *destination* */
    int mask_is_ipset; /**< @brief *destination* is ipset  */
    struct _firewall_rule_t *next;
} t_firewall_rule;

typedef struct _firewall_ruleset_t {
    char *name;
    t_firewall_rule *rules;
    struct _firewall_ruleset_t *next;
} t_firewall_ruleset;

/**
 * Trusted MAC Addresses
 */
typedef struct _trusted_mac_t {
    char *mac;
    struct _trusted_mac_t *next;
} t_trusted_mac;

/**
 * Popular Servers
 */
typedef struct _popular_server_t {
    char *hostname;
    struct _popular_server_t *next;
} t_popular_server;


typedef struct whitelist_struct {
	char *url;
	struct whitelist_struct *next;
}whitelist_struct_t;


typedef struct blacklist_struct {
	char *url;
	struct blacklist_struct *next;
}blacklist_struct_t;



/**
 * Configuration structure
 */
typedef struct gsconfig{

	router_struct_t *router;
	base_struct_t *base;
	internet_struct_t *internet;
	safe_struct_t *safe;

	t_auth_serv *auth_servers;  /**< @brief Auth servers list */
    t_firewall_ruleset *rulesets;       /**< @brief firewall rules */
    t_trusted_mac *trustedmaclist; /**< @brief list of trusted macs */
    t_popular_server *popular_servers; /**< @brief list of popular servers */

    blacklist_struct_t *blacklist;
    whitelist_struct_t *whitelist;


} s_config;






/** @brief Get the current gateway configuration */
s_config *config_get_gsconfig(void);

/** @brief Reads the configuration file */
int gsconfig_read();
/** @brief Check that the configuration is valid */
void gsconfig_validate(void);


/** @brief Get the active auth server */
t_auth_serv *get_auth_server(void);

/** @brief Bump server to bottom of the list */
void mark_auth_server_bad(t_auth_serv *);

/** @brief Fetch a firewall rule set. */
t_firewall_rule *get_ruleset(const char *);

interval_struct_t *get_intervalconfig();

#define LOCK_CONFIG() do { \
	debug(LOG_DEBUG, "Locking config"); \
	pthread_mutex_lock(&config_mutex); \
	debug(LOG_DEBUG, "Config locked"); \
} while (0)

#define UNLOCK_CONFIG() do { \
	debug(LOG_DEBUG, "Unlocking config"); \
	pthread_mutex_unlock(&config_mutex); \
	debug(LOG_DEBUG, "Config unlocked"); \
} while (0)

#endif                          /* _CONFIG_H_ */

int save_config_json_to_file(cJSON *config_j,char *filepath);
cJSON * router_struct_to_j();


/** @internal
 * Validate that popular servers are populated or log a warning and set a default.
 */
static void
validate_popular_servers(void);

cJSON *get_config_j();
void gsconfig_struct_to_j();

router_struct_t *get_routerconfig();

cJSON * get_ping_router_struct_to_j();


void whitelist_j_to_struct(cJSON *p);
void blacklist_j_to_struct(cJSON *p);

/*SSID */
int ssid_j_to_struct_cause_boot(cJSON *p);
//设置IMEI
int set_imei();



