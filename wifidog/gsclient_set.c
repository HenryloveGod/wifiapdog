
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <syslog.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
//#include <wait.h>
#include <sys/types.h>
#include <string.h>
#include <sys/types.h>
#include <sys/un.h>
#include <unistd.h>
#include <syslog.h>
#include <errno.h>
#include <iconv.h>

#include "common.h"
#include "wdctl_thread.h"
#include "safe.h"
#include "gsconfig.h"
#include "fw_iptables.h"
#include "firewall.h"
#include "debug.h"
#include "util.h"
#include "client_list.h"
#include "gscJSON.h"
#include "gateway.h"
#include "gsclient_handle.h"
#include "wdctl_thread.h"
#include "gs_sms.h"
#include "gsclient_set.h"
#include "gsbasemethod.h"


#define CHAIN_UNTRUSTED "CHAIN_UNTRUSTED"

char *net_set(cJSON *p)
{
	char * rc;
	char *res="";
	cJSON *net_j;
	if(NULL == p)
		return "PARA IS NULL";
	net_j = p->child;
	if(NULL ==  net_j)
		return "PARA IS NULL";
	while(net_j){
		if(strcmp(net_j->string,"download") ==0 ){
			rc = download_limit(net_j);
		}else if (strcmp(net_j->string,"upload") ==0 ){
			rc = upload_limit(net_j);
		}else if (strcmp(net_j->string,"download_per") ==0 ){
			rc = download_limit_per_ip(net_j);
		}else if (strcmp(net_j->string,"upload_per") ==0 ){
			rc = upload_limit_per_ip(net_j);
		}
		safe_asprintf(&res,"%s;%s:%s",res,net_j->string,rc);
		net_j = net_j->next;
	}
	return res;
}

int set_psswd(cJSON *p)
{
	char *newuser,*newpwd;
	newuser ="root";
	newpwd= NULL;

	if(cJSON_GetObjectItem(p,"username")){
		if(cJSON_GetObjectItem(p,"username")->valuestring){
			newuser = cJSON_GetObjectItem(p,"username")->valuestring;
			if(strcmp(newuser,config_get_gsconfig()->router->username) == 0){
				debug(LOG_NOTICE,"User only support root,not [%s]",newuser);
				newuser ="root"; //只支持root
			}
		}
	}
	if(cJSON_GetObjectItem(p,"password")){
		if(cJSON_GetObjectItem(p,"password")->valuestring){
			newpwd = cJSON_GetObjectItem(p,"password")->valuestring;
			if(strcmp(newpwd,config_get_gsconfig()->router->password) == 0){
				newpwd =NULL;
			}
		}
	}

	if(newuser && newpwd){
		char *cmd;
		safe_asprintf(&cmd,"lua /usr/bin/setpwd.lua %s %s",newuser,newpwd);
		debug(LOG_NOTICE,"password change command [%s]",cmd);
		return excute_cmd(cmd);
	}
	return 0;
}



char *router_base_set(cJSON *p)
{
	s_config *conf = config_get_gsconfig();

	set_string_member_to_struct(p,"portal",&(conf->router->portal));
	set_number_member_to_struct(p,"id", &(conf->router->id));
	ssid_j_to_struct_cause_boot(p);
	set_psswd(p);
	set_string_member_to_struct(p,"key",&(conf->router->key));

	return "OK";
}

//整体下载限速
char * download_limit(cJSON *p)
{
	if(!p)
		return "para is empty";
	
	iptables_do_command(" -F INPUT");
	debug(LOG_NOTICE,"download_limit set to %d",p->valuedouble);
	int rc = iptables_do_command("-A INPUT -m limit --limit %d/s -j ACCEPT" ,p->valuedouble);
	if(rc == 0 )
		return "OK";
	else
		return "FAIL";
}

//整体上传限速
char * upload_limit(cJSON *p)
{
	if(!p)
		return "PARAERROR";
	if(p->valuedouble <= 0 )
		return "PARAERROR";
	iptables_do_command(" -F OUTPUT");
	debug(LOG_NOTICE,"upload_limit set to %d",p->valuedouble);
	int rc = iptables_do_command("-A OUTPUT -m limit --limit %d/s -j ACCEPT" , p->valuedouble);
	if(rc == -1 )
		return "FAIL";
	return "OK";

}

//单IP下载限速
char * download_limit_per_ip(cJSON *p)
{
	if(!p)
		return "PARAERROR";
	int i,rc;
	char *res;
	cJSON *tmp_j;
	res = "(";
	i = 0;
	while((tmp_j=cJSON_GetArrayItem(p,i)) != NULL){
		if(tmp_j->child){
			char *ip = tmp_j->child->string;
			int speed =  tmp_j->child->valuedouble;
			debug(LOG_NOTICE,"download_limit_per_ip %s set to %d",ip,speed);
			rc = iptables_do_command("-A FORWARD -d %s -m limit --limit %d/s -j ACCEPT" ,ip , speed);
			if(rc == 0)
				safe_asprintf(&res,"%s%s:OK;",res,ip);
			else
				safe_asprintf(&res,"%s%s:FAIL%d;",res,ip,rc);
		}else
			safe_asprintf(&res,"%sformaterror;",res);
		i++;
	}
	safe_asprintf(&res,"%s)",res);
	if(NULL == res)
		res = "FORMATERROR?";
	return res;
}


//单IP上传限速
char * upload_limit_per_ip(cJSON *p)
{
	if(NULL == p)
		return "PARAERROR";
	int i,rc;
	char *res;
	res = "(";

	cJSON *tmp_j;
	i = 0;
	while((tmp_j=cJSON_GetArrayItem(p,i)) != NULL){
		if(tmp_j->child){
			char *ip = tmp_j->child->string;
			int speed =  tmp_j->child->valuedouble;
			debug(LOG_NOTICE,"upload_limit_per_ip %s set to %d",ip,speed);
			rc = iptables_do_command("-A FORWARD  -s %s -m limit --limit %d/s -j ACCEPT",ip , speed);
			if(rc == 0)
				safe_asprintf(&res,"%s%s:OK;",res,ip);
			else
				safe_asprintf(&res,"%s%s:FAIL%d;",res,ip,rc);
		}else
			safe_asprintf(&res,"%sformaterror;",res);
		i++;
	}
	safe_asprintf(&res,"%s)",res);
	if(NULL == res)
		res = "FORMATERROR";
	return res;

}

/* 禁止上网，除服务器外
char * disable_net_except_eotu(para_struct_t *p)
{
	iptables_do_command("-F");
	iptables_do_command("-X");
	iptables_do_command("-Z");
	iptables_do_command("-P INPUT DROP");
	iptables_do_command("-P FORWARD DROP");
	iptables_do_command("-P OUTPUT DROP");
	iptables_do_command("-I INPUT -p all -s %s -j ACCEPT" , p->value);
	iptables_do_command("-I INPUT -p all -d %s -j ACCEPT" , p->value);
	iptables_do_command("-I OUTPUT -p all -s %s -j ACCEPT" , p->value);
	iptables_do_command("-I OUTPUT -p all -d %s -j ACCEPT" , p->value);
	iptables_do_command("-I FORWARD -p all -s %s -j ACCEPT" , p->value);
	iptables_do_command("-I FORWARD -p all -d %s -j ACCEPT" , p->value);
	return "OK";
}

//网站白名单MAC设置
char * net_whitelist_mac_set(para_struct_t * p)
{
	iptables_do_command("-t mangle -A " CHAIN_TRUSTED " -m mac --mac-source %s -j MARK --set-mark %d",tmp_p->value, FW_MARK_KNOWN);
	return "NOUSE";
}

//网站黑名单MAC设置
char * net_blacklist_mac_set(cJSON *p)
{
iptables_do_command("-t mangle -A %s -m mac --mac-source %s -j	MARK --set-mark %d",CHAIN_UNTRUSTED ,  url, FW_MARK_KNOWN);
	return "NOUSE";
}
*/
//网站白名单域名URL设置
char * net_whitelist_url_set(cJSON *p)
{
	if(NULL == p)
		return "PARA EMPTY";
	whitelist_j_to_struct(p);
	whitelist_struct_t *wt;
	wt = config_get_gsconfig()->whitelist;
	while(wt){
		if(wt->url){
			debug(LOG_NOTICE,"net_whitelist_url_set %s",wt->url);
			iptables_do_command("-t nat -A " CHAIN_GLOBAL " -d %s -p tcp -j ACCEPT",wt->url);
			iptables_do_command("-t filter -A " CHAIN_GLOBAL " -d %s -p tcp -j ACCEPT",wt->url);
		}
		wt=wt->next;
	}
	return "OK";
}

//网站黑名单域名URL设置
char * net_blacklist_url_set(cJSON *p)
{

	if(NULL == p)
		return "PARA EMPTY";
	blacklist_j_to_struct(p);
	blacklist_struct_t *wt;
	wt = config_get_gsconfig()->blacklist;
	while(wt){
		if(wt->url){
			debug(LOG_NOTICE,"net_blacklist_url_set %s",wt->url);
        	iptables_do_command("-t filter -A " CHAIN_GLOBAL " -d %s -p tcp -j REJECT",wt->url);
		}
		wt=wt->next;
	}
	return "OK";
}

cJSON * message_thread_handle(cJSON *p,char *smsc)
{

	char *res;
	char *phone,*message;
	int  id;
	res =NULL;

	cJSON *res_j=NULL;
	res_j = cJSON_CreateObject();

	//判断参数是否正确------------------------------------------------------
	if(NULL == cJSON_GetArrayItem(p,0) || NULL == cJSON_GetArrayItem(p,1) || NULL == cJSON_GetArrayItem(p,2)){
		cJSON_AddStringToObject(res_j,(char *)id,"PARA EMPTY");
		return res_j;
	}
	id = cJSON_GetArrayItem(p,0)->valuedouble;
	phone = cJSON_GetArrayItem(p,1)->valuestring;
	message = cJSON_GetArrayItem(p,2)->valuestring;

	if(id ==0 || phone == NULL || message == NULL){
		if(id !=0)
			cJSON_AddStringToObject(res_j,(char *)id,"PARA ERROR");
		else
			cJSON_AddStringToObject(res_j,"0","PARA ERROR");
		return res_j;
	}

	//判断手机号是否合法
	safe_asprintf(&phone,"86%s",phone);
	if(strlen(phone) !=13){
		cJSON_AddStringToObject(res_j,(char *)id,"PHONE ERROR");
		return res_j;
	}

	//发送短信
	res = sms_tool(smsc,phone,message);
	//返回发送结果
	char *idstr;
	safe_asprintf(&idstr,"%d",id);
	cJSON_AddStringToObject(res_j,idstr,res);

	return res_j;
}


char * send_message(cJSON *p)
{
	if(p ==NULL)
		return "FAIL";
	char *smsc ;
	char *res="";
	int i=0;
	cJSON *tmp_para_j;
	cJSON *rj=NULL,*tj;

	//IO口初始化
	sms_init(config_get_gsconfig()->base->sms_portf);

	if(get_intervalconfig()->is_sms_work == 0){
		debug(LOG_ERR,"send sms stop for port error",config_get_gsconfig()->base->sms_portf);
		safe_asprintf(&res,"%s sms port open error",config_get_gsconfig()->base->sms_portf);
		return res;
	}


	//读取每条短信，发送
	while((tmp_para_j=cJSON_GetArrayItem(p,i)) != NULL){
		tj = message_thread_handle(tmp_para_j,config_get_gsconfig()->base->smsc);
		if(rj==NULL)
			rj=tj;
		else
			rj->next = tj;
		i++;
	}

	if(NULL== rj)
		rj = cJSON_CreateObject();
	return cJSON_Print(rj);
}


char *rsync_file(cJSON *p)
{

//command	rsync -vzrtopg --delete --password-file=/etc/rsync.pwd  113.195.207.216::www ./twofiles/
	char * cmd;
	if(NULL == p )
		return "paras is NULL";

	cJSON *tmp_para_j;
	int i=0;
	while((tmp_para_j=cJSON_GetArrayItem(p,i)) != NULL){
		if(NULL == tmp_para_j->valuestring)
			return "json format error";
		char *dest;
		dest=safe_strdup(tmp_para_j->valuestring);
		strsep(&dest,":");
		if(NULL != dest)
			strsep(&dest,":");
		else
			return "para : error";
		if(NULL == dest)
			return "no path to reserve";
		safe_asprintf(&cmd,"mkdir -p /mnt/eotusd/%s",dest);
		excute_cmd(cmd);
		safe_asprintf(&cmd,"rsync -vzrtopg --delete --password-file=/etc/rsync.pwd  %s /mnt/eotusd/%s",tmp_para_j->valuestring,dest);
		debug(LOG_NOTICE,"New Thread : %s",cmd);
		command_run_thread(cmd);
		i++;
	}

	return "another thread will response";
}

/*安装软件*/
char * opkg_install_ipk(cJSON *p)
{
	char *cmd;

	safe_asprintf(&cmd, "opkg install %s", p->valuestring);
	debug(LOG_NOTICE,"%s",cmd);
	int rc;
	rc = excute_cmd(cmd);
	if(rc == 0)
		return "ok";
	else
		return "fail";
}
/*更新固件*/
char * sysupgrade_firmware(cJSON *p)
{
	if(!p)
		return "para is NULl,ERROR!";
	return upgrade_firmware(p);

}





/*wifi 无线 AP模式设置*/

char * wifi_ap_mode_set(cJSON *p)
{
	char *res ="";
	int rc;
	char *ssid,*encr,*pswd;
	if(NULL == p )
		return "p NULL";
	
	if(NULL != cJSON_GetArrayItem(p,0)){
		ssid = cJSON_GetArrayItem(p,0)->valuestring;
		char *oldssid = get_popen_str("uci get wireless.apmode.ssid");
		//if(strstr(ssid,oldssid) >0 )
		//	get_intervalconfig()->bootv=2;
		rc = wifi_ap_member_set("ssid",ssid);
		safe_asprintf(&res,"%s%s:%d;",res,"ssid",rc);
	}
	if(NULL != cJSON_GetArrayItem(p,1)){
		encr = cJSON_GetArrayItem(p,1)->valuestring;
		rc = wifi_ap_member_set("encryption",encr);
		safe_asprintf(&res,"%s%s:%d;",res,"encryption",rc);
	}
	if(NULL != cJSON_GetArrayItem(p,2)){
		pswd = cJSON_GetArrayItem(p,2)->valuestring;
		rc = wifi_ap_member_set("key",pswd);
		safe_asprintf(&res,"%s%s:%d;",res,"password",rc);
	}

	if(NULL != cJSON_GetArrayItem(p,3)){
		char *pwer;
		pswd = cJSON_GetArrayItem(p,3)->valuestring;
		safe_asprintf(&pwer,"uci set wireless.radio0.power=%s",cJSON_GetArrayItem(p,3)->valuestring);
		rc = excute_cmd(pwer);
		safe_asprintf(&res,"%s%s:%d;",res,"power",rc);
	}

	if( 0 != wifi_ap_commit_apply ){
		safe_asprintf(&res,"%s;cmd[uci commit wireless] ERROR!",res);
		debug(LOG_ERR,res);
	}
	return res;
}

/*wifi 无线 STA模式设置*/
char * wifi_sta_mode_set(cJSON *p)
{
	char *res,*cmd;
	int rc;
	res ="";
	char *ssid,*mac,*encr,*pswd,*channel;
	if(NULL == p )
		return "ssid NULL";

	if(NULL != cJSON_GetArrayItem(p,0)){
		ssid = cJSON_GetArrayItem(p,0)->valuestring;
		rc = wifi_sta_member_set("ssid",ssid);
		safe_asprintf(&res,"%s%s:%d;",res,"ssid",rc);
	}
	if(NULL != cJSON_GetArrayItem(p,1)){
		mac = cJSON_GetArrayItem(p,1)->valuestring;
		rc = wifi_sta_member_set("bssid",mac);
		safe_asprintf(&res,"%s%s:%d;",res,"encryption",rc);
	}
	if(NULL != cJSON_GetArrayItem(p,2)){
		encr = cJSON_GetArrayItem(p,2)->valuestring;
		rc = wifi_sta_member_set("encryption",encr);
		safe_asprintf(&res,"%s%s:%d;",res,"encryption",rc);
	}
	if(NULL != cJSON_GetArrayItem(p,3)){
		pswd = cJSON_GetArrayItem(p,3)->valuestring;
		rc = wifi_sta_member_set("key",pswd);
		safe_asprintf(&res,"%s%s:%d;",res,"password",rc);
	}
	if(NULL != cJSON_GetArrayItem(p,4)){
		channel = cJSON_GetArrayItem(p,4)->valuestring;
		safe_asprintf(&cmd,"uci set wireless.radio0.channel=%s",channel);
		debug(LOG_NOTICE,"%s",cmd);
		rc = excute_cmd(cmd);
		safe_asprintf(&res,"%s%s:%d;",res,"password",rc);
	}

	debug(LOG_NOTICE,"uci commit wireless");
	if( -1 == excute_cmd("uci commit wireless") ){
		safe_asprintf(&res,"%s;cmd[uci commit wireless] ERROR!",res);
		debug(LOG_ERR,res);
	}

	return res;
}

/**************************************
*	system restart  
***************************************/


char *system_restart(cJSON *p)
{
    gsconfig_struct_to_j();
    save_config_json_to_file(get_config_j(),config_get_gsconfig()->base->configfile);
    debug(LOG_NOTICE,"Run command system reboot!");
	excute_cmd("reboot");
	return "system_restart";

}
/**************************************
*	network restart 
***************************************/

char *network_restart(cJSON *p)
{
    gsconfig_struct_to_j();
    save_config_json_to_file(get_config_j(),config_get_gsconfig()->base->configfile);
    debug(LOG_NOTICE,"Run command Network restart");
	excute_cmd("/etc/init.d/network restart");
	return "network_restart";

}

/**************************************
*	eotu restart 
***************************************/

static size_t
send_request(int sock, const char *request)
{
    size_t len;
    ssize_t written;

    len = 0;
    while (len != strlen(request)) {
        written = write(sock, (request + len), strlen(request) - len);
        if (written == -1) {
            fprintf(stderr, "Write to wifidog failed: %s\n", strerror(errno));
            exit(1);
        }
        len += (size_t) written;
    }

    return len;
}


static int
connect_to_server(const char *sock_name)
{
    int sock;
    struct sockaddr_un sa_un;

    /* Connect to socket */
    sock = socket(AF_UNIX, SOCK_STREAM, 0);
    if (sock < 0) {
        fprintf(stderr, "wdctl: could not get socket (Error: %s)\n", strerror(errno));
        exit(1);
    }
    memset(&sa_un, 0, sizeof(sa_un));
    sa_un.sun_family = AF_UNIX;
    strncpy(sa_un.sun_path, sock_name, (sizeof(sa_un.sun_path) - 1));

    if (connect(sock, (struct sockaddr *)&sa_un, strlen(sa_un.sun_path) + sizeof(sa_un.sun_family))) {
        fprintf(stderr, "wdctl: wifidog probably not started (Error: %s)\n", strerror(errno));
        exit(1);
    }

    return sock;
}


char * wdctl_eotu_restart(cJSON *p)
{
    int sock;
    char buffer[4096];
    char request[16];
    ssize_t len;
	
    /*save config before restart*/
    gsconfig_struct_to_j();
    save_config_json_to_file(get_config_j(),config_get_gsconfig()->base->configfile);

	s_config config = *(config_get_gsconfig());
    sock = connect_to_server(config.base->wdctl_sock);
    strncpy(request, "restart\r\n\r\n", 10);
    debug(LOG_NOTICE,"send signal to restart");
    send_request(sock, request);

    while ((len = read(sock, buffer, sizeof(buffer) - 1)) > 0) {
        buffer[len] = '\0';
        fprintf(stdout, "%s", buffer);
    }

    shutdown(sock, 2);
    close(sock);

	return "unexcept return value after wdctl_eotu_restart";
}


char *wdctl_eotu_stop(cJSON *p)
{

    gsconfig_struct_to_j();
    save_config_json_to_file(get_config_j(),config_get_gsconfig()->base->configfile);
    debug(LOG_NOTICE,"save config , then self kill");
	wdctl_stop(1);
	return "unexcept return after wdctl_eotu_stop";
}

