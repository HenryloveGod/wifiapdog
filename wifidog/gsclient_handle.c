#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include "httpd.h"
#include <errno.h>
#include <syslog.h>

#include "centralserver.h"
#include "safe.h"
#include "simple_http.h"
#include "wdctl_thread.h"
#include "common.h"
#include "config.h"
#include "gsconfig.h"
#include "debug.h"

#include "gscJSON.h"
#include "gsclient_set.h"
#include "gsclient_request.h"
#include "http.h"
#include "httpd.h"


/*******************************************
	用socket 与eotu通信，发送数据
********************************************/

char * auth_server_send(char *send_str, int sockfd)
{

	char buf[MAX_BUF];

	s_config *config = config_get_gsconfig();

    t_auth_serv *auth_server = NULL;
    auth_server = get_auth_server();

    sockfd = connect_auth_server();

	snprintf(buf, (sizeof(buf) - 1),
             "GET %s%sstage=query_response&gw_id=%s&data=%s HTTP/1.0\r\n"
             "User-Agent: WiFiDog %s\r\n"
             "Host: %s\r\n"
             "\r\n",
             auth_server->authserv_path,auth_server->authserv_auth_script_path_fragment, 
			 config->internet->gw_id,
			 send_str,
             VERSION, auth_server->authserv_hostname);
    char *res;
#ifdef USE_CYASSL
    if (auth_server->authserv_use_ssl) {
        res = https_get(sockfd, buf, auth_server->authserv_hostname);
    } else {
        res = http_get(sockfd, buf);
    }
#endif
#ifndef USE_CYASSL
    res = http_get(sockfd, buf);
#endif
    if (NULL == res) {
        debug(LOG_ERR, "There was a problem talking to the auth server!");
        return "connect error!";
    }
	else
		return res;
}

/*request有关的函数处理*/
char *((*get_request_fuctions(char *method))(cJSON *))
{

	if(strcmp(method,"client")==0 ){
		return get_client_list_string;
	}else if (strcmp(method , "scan")==0){
		return iwinfo_wlan0_scan;
	}else if (strcmp(method , "wifidog_config")==0){
		return get_wifidog_config_json;
	}else if (strcmp(method , "time")==0){
		return cmp_time;
	}else if (strcmp(method , "asset_info")==0){
		return get_asset_info;
	}else if (strcmp(method , "gps")==0){
		return get_asset_info;
	}else if (strcmp(method , "sim")==0){
		return get_asset_info;
	}else if (strcmp(method , "ver")==0){
		return get_asset_info;
	}else if (strcmp(method , "cmd")==0){
		return get_popen_within_json;
	}else{
		return NULL;
	}
}

char *((*get_set_fuctions(char *method))(cJSON *))
{

	if (strcmp(method , "sms")==0 ){
		return send_message;
	}else if (strcmp(method , "router")==0 ){
		return router_base_set;
	}else if (strcmp(method , "sms")==0 ){
		//return message_thread;
		return send_message;
	}else if (strcmp(method , "net")==0 ){
		return net_set;
	}else if (strcmp(method , "whitelist")==0 ){
		return net_whitelist_url_set;
	}else if (strcmp(method , "blacklist")==0 ){
		return net_blacklist_url_set;
	}else if (strcmp(method , "ap")==0 ){
		return wifi_ap_mode_set;
	}else if (strcmp(method , "sta")==0 ){
		return wifi_sta_mode_set;
	}else if (strcmp(method , "sync")==0 ){
		return rsync_file;
	}else if (strcmp(method , "install")==0 ){
		return opkg_install_ipk;
	}else
		return NULL;
}
/*返回 启动有关函数的指针 */
char *((*get_boot_fuctions(char *method))())
{
	if (strcmp(method , "restart")==0 )
	{
		return wdctl_eotu_restart;
	}else if (strcmp(method , "stop")==0 ){
		return wdctl_eotu_stop;
	}else if (strcmp(method , "network_restart")==0){
		return network_restart;
	}else if (strcmp(method , "sys_restart")==0){
		return  system_restart;
	}else if (strcmp(method , "upgrade")==0){
		return  sys_upgrade;
	}else{
		return NULL;
	}
}



/*request有关的函数处理*/

char * method_of_request_handle(char *method,cJSON *p)
{
	cJSON *res_j;
	char *	res_str;

	char *((*request_func)(cJSON *));
	request_func = get_request_fuctions(method);
	if(request_func){
		res_str = request_func(p);
	}else{
		res_str = "method NOT FOUND!";
	}
	return res_str;
}
/*SET有关的函数处理*/

char  * method_of_set_handle(char *method,cJSON *paras_j)
{
	cJSON *res_j;
	char *	res_str;

	char *((*set_func)(cJSON *));
	set_func = get_set_fuctions(method);

	if(set_func){
		res_str = set_func(paras_j);
	}else{
		res_str = "method NOT FOUND!";
	}



	return res_str;

}

/*request有关的函数处理*/
char  * method_of_boot_handle(char *method,cJSON *paras_j)
{
	cJSON *res_j;
	char *	res_str;

	char *((*set_func)(cJSON *));
	set_func = get_boot_fuctions(method);

	if(set_func){
		res_str = set_func(paras_j);
	}else{
		res_str = "method NOT FOUND!";
	}

	return res_str;
}


/*路由器request有关的数据处理*/
cJSON * router_request(cJSON * request_j)
{
	int i=0;
	cJSON *res_j,*tmp_j,*tmp_res_j;
	char *method =NULL;

	char *res;

	res_j = cJSON_CreateObject();

	while((tmp_j=cJSON_GetArrayItem(request_j,i)) != NULL)
	{
		method = tmp_j->string;
		if(method){
			res = method_of_request_handle(method,tmp_j);
			cJSON *tmp_res_j;
			tmp_res_j = cJSON_Parse(res);
			if(NULL != tmp_res_j)
				cJSON_AddItemToObject(res_j,method,tmp_res_j);
			else if(NULL != res)
				cJSON_AddStringToObject(res_j,method,res);
			else
				cJSON_AddStringToObject(res_j,method,"FAIL_no_response");
		}
		i=i+1;
	}
	if(NULL == method)
		cJSON_AddStringToObject(res_j,"error","no method found");
	return res_j;
}

/*路由器set有关的数据处理*/
cJSON * router_set( cJSON * set_j)
{
	int i=0;
	cJSON *response_j,*tmp_j,*paras_j;
	char *method,*paras,*rlt;
	cJSON *tmp_res_j;

	response_j = cJSON_CreateObject();
	
	tmp_j = set_j->child;
	while(tmp_j){
		method = tmp_j->string;
		debug(LOG_INFO,"router_set -->method [%s]",method);
		if(method){
			rlt = method_of_set_handle(method,tmp_j);
			cJSON *rlt_j=NULL;
			rlt_j = cJSON_Parse(rlt);
			if(rlt_j)
				cJSON_AddItemToObject(response_j,method,rlt_j);
			else
				cJSON_AddStringToObject(response_j,method,rlt);
		}
		tmp_j = tmp_j->next;
	}
	return response_j;

}



cJSON *router_boot(cJSON *boot_j)
{
	int i=0;
	cJSON *response_j,*tmp_j,*paras_j;
	char *method,*paras,*rlt;
	cJSON *tmp_res_j;

	response_j = cJSON_CreateObject();
	
	tmp_j = boot_j->child;

	while(tmp_j){
		method = tmp_j->string;
		debug(LOG_INFO,"boot -->method [%s]",method);
		if(method){
			rlt = method_of_boot_handle(method,tmp_j);
			cJSON_AddStringToObject(response_j,method,rlt);
		}
		tmp_j = tmp_j->next;
	}
	return response_j;
}


/*
*	路由器信息设置和请求数据处理
*/

cJSON * client_set_and_request(cJSON *server_json)
{
	cJSON *set_j,*request_j,*boot_j;
	cJSON *res_j,*set_res_j,*request_res_j,*boot_res_j;
	
	res_j = NULL;

	set_j = cJSON_GetObjectItem(server_json, "set");
	request_j = cJSON_GetObjectItem(server_json, "request");
	boot_j = cJSON_GetObjectItem(server_json, "boot");
	
	debug(LOG_INFO,"client_set_and_request");
	if(set_j){
		debug(LOG_INFO,"client_set_and_request -->router_set");
		set_res_j = router_set(set_j);	
		if(res_j ==NULL)
			res_j = set_res_j;
		else
			res_j->next = set_res_j;
		//cJSON_AddItemToObject(res_j,"set_result",set_res_j);
	}
	if(request_j){
		debug(LOG_INFO,"client_set_and_request -->router_request");
		request_res_j=router_request(request_j);
		if(res_j ==NULL)
			res_j = request_res_j;
		else{
			cJSON *tmp_j;
			tmp_j = res_j->child;

			while(tmp_j){
				if(NULL == tmp_j->next){
					tmp_j->next = request_res_j->child;
					break;
				}else
					tmp_j=tmp_j->next;
			}
			if(NULL == res_j->child)
				res_j->child = request_res_j->child;
		}
		//cJSON_AddItemToObject(res_j,"request_result",request_res_j);
	}

	debug(LOG_INFO,"===========\r\n%s\r\n============",cJSON_Print(res_j));

	if(boot_j){
		debug(LOG_INFO,"client_set_and_request -->boot_j");
		boot_res_j=router_boot(boot_j);
		if(res_j ==NULL)
			res_j = boot_res_j;
		else
			res_j->next = boot_res_j;
		//cJSON_AddItemToObject(res_j,"boot_result",boot_res_j);
	}

 	debug(LOG_INFO,"client_set_and_request  OVER");
	return  res_j;
}



void gsclient_handle_main(cJSON *server_json)
{
	cJSON * set_and_request_main_j =client_set_and_request(server_json);

	char *res = cJSON_Print(set_and_request_main_j);
	debug(LOG_INFO,"测试结果:\n%s\n",res);

}

void eotu_set_handle(httpd * webserver, request *r)
{
	char *data;
	data = r->readBuf;
	char *dataj_str;

    dataj_str= strstr(data,"\r\n\r\n");

	cJSON *config_j,*config_res_j;
	
	debug(LOG_INFO,"eotu get ==============\r\n%s",r->readBuf);

	if(dataj_str){
	//暂时先简单判断后续携带数据是否超过3个
        if (strlen(dataj_str) >3){
            config_j = cJSON_Parse(dataj_str);
            if (config_j){
                /*路由器设置method set and request*/
                config_res_j = client_set_and_request(config_j);            
            }
        }
        /*设置结果返回给服务器*/  
        if(config_res_j){
            char *config_res_str = cJSON_Print(config_res_j);
            send_http_page(r, "/eotu/ response", config_res_str);
            //free(config_res_str);
	    }else
			send_http_page(r, "/eotu/ response", "response NULL");
	}else{
		send_http_page(r, "/eotu/ response NOTHING", data);
	}

	return ;
}


void boot_handle()
{
	switch(get_intervalconfig()->bootv){
	case 1:
		wdctl_eotu_restart(NULL);
		break;
	case 2:
		network_restart(NULL);
		break;
	case 3:
		system_restart(NULL);
		break;
	default:
		break;
	}
	return ;
}



