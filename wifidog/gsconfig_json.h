


#include "gscJSON.h"

#define DEFAULT_CONFIGFILE_JSON "/etc/wifidog.conf.json"
#define DEFAULT_ASSETFILE_JSON "/etc/wifidog.asset.json"
#define DEFAULT_EXTRAFILE_JSON "/etc/wifidog.extra.json"
#define DEFAULT_TRANSPORTFILE_JSON "/etc/wifidog.file.json"
#define DEFAULT_ALERTFILE_JSON "/etc/wifidog.alert.json"
#define DEFAULT_DATACONTROLFILE_JSON "/etc/wifidog.datacontrol.json"
#define EOTUREQUEST "/eotu/request/"

#define REGISTER_FILE_PATH "/etc/eotu.reg"

#define AUTH_MAX_DOWN_QTY	20

//AUTH_MAX_DOWN_QTY,超过心跳次数没有链接服务器，自动重启

typedef struct gsconfig_st{
	
	int max_total_usage_int;
	int max_users_qty_online_limit_int;
	char *eoturequestpath;
	int auth_down_qty;
        int is_need_register; // 0 no ,1 yes

}gsconfig_struct_t;


void gsconfig_struct_init(void);

gsconfig_struct_t *config_get_gsconfig_struct();


cJSON * config_get_extra_config_j(void);

cJSON * config_get_gsconfig_j(void);

/****************************************************************************************************
*	从文件中读取字符串，parse到config_json格式，部分设置到config_struct中，其他设置到extra_config_j中
*****************************************************************************************************/
void config_json_read(char *filename);

/*********************************
*
*	新增功能extra_config_j 数据应用
*	1>	上网流量控制
*	2>	短信发送
*	3>	文件传输
*	4>	命令执行
***********************************/
int extra_config_fw_deploy(cJSON *config_j);


/*****************************************************
*	把config_struct和extra_config_j整合到config_json中
******************************************************/

cJSON * set_struct_and_extra_to_config_json();


/*保存当前的config_json到文件中*/

int save_config_json_to_file(cJSON *config_j,char *filepath);

