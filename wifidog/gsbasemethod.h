
#include "gscJSON.h"
#include "gsconfig.h"

char *get_router_id();
char *get_router_sign();
char *get_router_key();

unsigned long int get_sys_uptime();
unsigned int get_sys_memfree();
float get_sys_load();
char *get_firmware_version();
char *get_dog_version();
int save_time_to_config_j(cJSON *config_j,char *member,char *string);

cJSON *get_status_j();
cJSON *get_ping_key_id_info();

char *get_portal_url();
int set_string_ajson_to_bjson(cJSON *a_j,char *amember,cJSON *b_j,char *bmember);
char *build_sign(char *gw_id,char *key,char *updatetime);

void check_gsconfig_with_system();
char *get_dongle_imei();
/*获取毫秒级时间*/
char *get_nano_second();
cJSON *get_sms_ping_j();
/*创建新进程运行cmd*/
char * command_run_thread(char *p);

char *get_http_res(t_auth_serv *auth_server,int sockfd,char *request );

char *get_new_sign_by_id_gwid_time(long time);

int wifi_ap_member_set(char *member,char *value);
int wifi_sta_member_set(char *member,char *value);

int wifi_ap_commit_apply();
/*更新固件*/
char * upgrade_firmware(cJSON *p);
