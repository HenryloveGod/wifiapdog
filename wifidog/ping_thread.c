/* vim: set sw=4 ts=4 sts=4 et : */
/********************************************************************\
 * This program is free software; you can redistribute it and/or    *
 * modify it under the terms of the GNU General Public License as   *
 * published by the Free Software Foundation; either version 2 of   *
 * the License, or (at your option) any later version.              *
 *                                                                  *
 * This program is distributed in the hope that it will be useful,  *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of   *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    *
 * GNU General Public License for more details.                     *
 *                                                                  *
 * You should have received a copy of the GNU General Public License*
 * along with this program; if not, contact:                        *
 *                                                                  *
 * Free Software Foundation           Voice:  +1-617-542-5942       *
 * 59 Temple Place - Suite 330        Fax:    +1-617-542-2652       *
 * Boston, MA  02111-1307,  USA       gnu@gnu.org                   *
 *                                                                  *
\********************************************************************/

/* $Id$ */
/** @file ping_thread.c
    @brief Periodically checks in with the central auth server so the auth
    server knows the gateway is still up.  Note that this is NOT how the gateway
    detects that the central server is still up.
    @author Copyright (C) 2004 Alexandre Carmel-Veilleux <acv@miniguru.ca>
*/

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <syslog.h>
#include <signal.h>
#include <errno.h>

#include "../config.h"
#include "safe.h"
#include "common.h"
#include "debug.h"
#include "ping_thread.h"
#include "util.h"
#include "centralserver.h"
#include "firewall.h"
#include "gateway.h"
#include "simple_http.h"

#include "gsconfig.h"
#include "gsbf.h"
#include "gsclient_handle.h"
#include "gsclient_request.h"
#include "gscJSON.h"
#include "gsbasemethod.h"


static void ping(void);


//中途检测一下文件数量是否超标
void debug_file_inter_check()
{
	file_pn_struct_t *pn = get_file_path(config_get_gsconfig()->base->processlog);
	file_pn_struct_t *cn = get_file_path(config_get_gsconfig()->base->clientslog);

	//检测日志文件数量，过量则删除
	delete_extra_debug_file(pn);
	delete_extra_debug_file(cn);

}




/** Launches a thread that periodically checks in with the wifidog auth server to perform heartbeat function.
@param arg NULL
@to do This thread loops infinitely, need a watchdog to verify that it is still running?
*/
void
thread_ping(void *arg)
{
    pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
    pthread_mutex_t cond_mutex = PTHREAD_MUTEX_INITIALIZER;
    struct timespec timeout;

    while (1) {
        /* Make sure we check the servers at the very begining */
        ping();

        debug_file_inter_check();

        /* Sleep for config.checkinterval seconds... */
        timeout.tv_sec = time(NULL) + config_get_gsconfig()->internet->checkinterval;
        timeout.tv_nsec = 0;
        /* Mutex must be locked for pthread_cond_timedwait... */
        pthread_mutex_lock(&cond_mutex);
        /* Thread safe "sleep" */
        pthread_cond_timedwait(&cond, &cond_mutex, &timeout);
        /* No longer needs to be locked */
        pthread_mutex_unlock(&cond_mutex);
    }
}


char *coin_ping(t_auth_serv *auth_server)
{
    char *id;
    char *sign;
    char *key;

    float sys_load = 0;
    char *status_line;
    cJSON *user_j,*ping_key_id_j;

    ping_key_id_j = get_ping_key_id_info();

    char *extra_string;
    char *request;
   
    user_j = get_client_list(NULL);
    //cJSON_AddItemToObject(ping_key_id_j,"user_info",user_j);
    //cJSON_AddItemToObject(ping_key_id_j,"sms",get_sms_ping_j());
    cJSON_AddItemToObject(ping_key_id_j,"status",get_status_j());
    //cJSON_AddNumberToObject(ping_key_id_j,"status",get_routerconfig()->status);
    cJSON_AddItemToObject(ping_key_id_j,"router",cJSON_Duplicate(get_ping_router_struct_to_j(),1));

    extra_string = safe_strdup(cJSON_Print(ping_key_id_j));
    extra_string = str_replace(extra_string,"\t","");
    extra_string = str_replace(extra_string,"\n","");
    //extra_string = str_replace(extra_string," ","");
    cJSON_Delete(ping_key_id_j);
    //auth_server->authserv_ping_script_path_fragment,
    safe_asprintf(&request,
             "POST %s%s HTTP/1.0\r\n"
             "User-Agent: EOTU %s\r\n"
             "Content-Type:application/json\r\n"
             "Host: %s\r\n"
             "content-length:%d\r\n\r\n"
             "%s",
             auth_server->authserv_path,
             auth_server->authserv_ping_script_path_fragment,
             VERSION, 
             auth_server->authserv_hostname,strlen(extra_string),
             extra_string);
             
    return request;
}

char *coin_ping_response(t_auth_serv *auth_server,char *reponse)
{
    char *request;

    //auth_server->authserv_ping_script_path_fragment,
    safe_asprintf(&request,
             "POST %s%s HTTP/1.0\r\n"
             "User-Agent: EOTU %s\r\n"
             "Content-Type:application/json\r\n"
             "Host: %s\r\n"
             "content-length:%d\r\n\r\n"
             "%s",
             auth_server->authserv_path,
             auth_server->authserv_response_script_path_fragment,
             VERSION,
             auth_server->authserv_hostname,strlen(reponse),
			 reponse);

    return request;
}


char *coin_common(char *reponse,char *path,char *host)
{
    char *request;
    //auth_server->authserv_ping_script_path_fragment,
    safe_asprintf(&request,
             "POST %s HTTP/1.0\r\n"
             "User-Agent: EOTU %s\r\n"
             "Content-Type:application/json\r\n"
             "Host: %s\r\n"
             "content-length:%d\r\n\r\n"
             "%s",
             path,
             VERSION,
			 host,strlen(reponse),
			 reponse);

    return request;
}

static void  ping_reponse(cJSON *back_j)
{
    char *request;
    int sockfd;
    char *is_ok=NULL;

    t_auth_serv *auth_server = NULL;
    auth_server = get_auth_server();
    int i=0;

    for(;is_ok ==NULL && i<10;i++){
    	cJSON *ping_key_id_j = get_ping_key_id_info();
    	if(NULL == back_j)
    		return ;
    	cJSON *tmp_j=ping_key_id_j->child;
    	while(tmp_j){
    		if(NULL == tmp_j->next){
    			tmp_j->next =back_j->child;
    			break;
    		}else
    		tmp_j = tmp_j->next;
    	}
    	char *backto_response;
    	char *tmpstr=cJSON_Print(ping_key_id_j);
    	tmpstr = str_replace(tmpstr," ","");
    	tmpstr = str_replace(tmpstr,"\r\n","");
    	tmpstr = str_replace(tmpstr,"\t","");
    	tmpstr = str_replace(tmpstr,"\n","");
    	backto_response = coin_ping_response(auth_server,tmpstr);
    	sockfd = connect_auth_server();
    	is_ok = get_http_res(auth_server,sockfd,backto_response);
    	debug(LOG_DEBUG, "ping_reponse:  \r\n%s\r\n",is_ok);
    	is_ok = strstr(is_ok,"\r\n\r\n");
    	is_ok = strstr(is_ok,"OK");
    	sleep(1);
    }
    debug(LOG_INFO,"=========%d",get_intervalconfig()->bootv);
    boot_handle();
    get_intervalconfig()->bootv = 0;
    return ;
}


void ping_handle(char *res)
{
	cJSON *ping_reback_j=NULL;
    debug(LOG_NOTICE, "Get Ping Response: \r\n%s\r\n" ,res);
    char *status_line;
    if (NULL == res) {
        debug(LOG_ERR, "There was a problem pinging the auth server!");
    } else{
        status_line = strsep(&res,"\n");
        if(strstr(status_line,"200") > 0){
            debug(LOG_DEBUG, "Auth Server connect, and give set json data");
            char *dataj_str;
            dataj_str = strstr(res,"\r\n\r\n");
            cJSON *config_j,*config_res_j;
            config_res_j =NULL;
            if(NULL == dataj_str ){
            	return ;
            }
            config_j = cJSON_Parse(dataj_str);
            debug(LOG_DEBUG, "data is:\r\n %s",dataj_str);
            if (config_j){
            	ping_reback_j = client_set_and_request(config_j);
            }else{
            	cJSON *ponse_j = cJSON_CreateObject();
            	cJSON_AddStringToObject(ponse_j,"Error","ReceiveFromServer");
            	ping_reback_j = ponse_j;
            }
            //free(dataj_str);
            /*设置结果返回给服务器*/
            if(ping_reback_j){
            	ping_reponse(ping_reback_j);
            }
            if(config_res_j)
            	cJSON_Delete(config_res_j);
        }
       else{
        debug(LOG_ERR, "unknow status code [%s]!",status_line);
        //auth_server_send("ping send unknow status code ");
       }
    }

}

static void ping(void)
{
    char *request;
    int sockfd;
    t_auth_serv *auth_server = NULL;
    auth_server = get_auth_server();
    static int authdown = 0;

    sockfd = connect_auth_server();
    if (sockfd == -1) {
        if (!authdown) {
            fw_set_authdown();
            authdown = 1;
        }
        return;
    }

    request = coin_ping(auth_server);

    char *res;
    res = get_http_res(auth_server,sockfd,request);

    pthread_t ponse;
	int result = pthread_create(&ponse, NULL, (void *)ping_handle, safe_strdup(res));
	if (result != 0) {
		debug(LOG_ERR, "FATAL: Failed to create a new thread (message_thread) - exiting");
		return ;
	}
	pthread_detach(ponse);
	return ;

}
