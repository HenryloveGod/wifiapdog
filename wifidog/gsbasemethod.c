/* vim: set sw=4 ts=4 sts=4 et : */
/********************************************************************\
 * This program is free software; you can redistribute it and/or    *
 * modify it under the terms of the GNU General Public License as   *
 * published by the Free Software Foundation; either version 2 of   *
 * the License, or (at your option) any later version.              *
 *                                                                  *
 * This program is distributed in the hope that it will be useful,  *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of   *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    *
 * GNU General Public License for more details.                     *
 *                                                                  *
 * You should have received a copy of the GNU General Public License*
 * along with this program; if not, contact:                        *
 *                                                                  *
 * Free Software Foundation           Voice:  +1-617-542-5942       *
 * 59 Temple Place - Suite 330        Fax:    +1-617-542-2652       *
 * Boston, MA  02111-1307,  USA       gnu@gnu.org                   *
 *                                                                  *
\********************************************************************/

/* $Id$ */
/** @file ping_thread.c
    @brief Periodically checks in with the central auth server so the auth
    server knows the gateway is still up.  Note that this is NOT how the gateway
    detects that the central server is still up.
    @author Copyright (C) 2004 Alexandre Carmel-Veilleux <acv@miniguru.ca>
*/

#define _GNU_SOURCE

#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <syslog.h>
#include <signal.h>
#include <errno.h>
#include <sys/stat.h>

#include "../config.h"
#include "safe.h"
#include "common.h"

#include "debug.h"
#include "ping_thread.h"
#include "util.h"
#include "centralserver.h"
#include "firewall.h"
#include "gateway.h"
#include "simple_http.h"

#include "gsbf.h"
#include "gsclient_handle.h"
#include "gsclient_request.h"
#include "gsbasemethod.h"
#include "md5.h"

/*
* Populate uptime, memfree and load
*/

unsigned long int get_sys_uptime()
{
    FILE *fh;
    unsigned long int sys_uptime = 0;
    if ((fh = fopen("/proc/uptime", "r"))) {
        if (fscanf(fh, "%lu", &sys_uptime) != 1)
            debug(LOG_CRIT, "Failed to read uptime");
        fclose(fh);
    }
    return sys_uptime;
}

unsigned int get_sys_memfree()
{
    FILE *fh;
    unsigned int sys_memfree = 0;
    if ((fh = fopen("/proc/meminfo", "r"))) {
        while (!feof(fh)) {
            if (fscanf(fh, "MemFree: %u", &sys_memfree) == 0) {
                /* Not on this line */
                while (!feof(fh) && fgetc(fh) != '\n') ;
            } else {
                /* Found it */
                break;
            }
        }
        fclose(fh);
    }
    return sys_memfree;
}

float get_sys_load()
{
    FILE *fh;
    float sys_load = 0;
    if ((fh = fopen("/proc/loadavg", "r"))) {
        if (fscanf(fh, "%f", &sys_load) != 1)
            debug(LOG_CRIT, "Failed to read loadavg");
        fclose(fh);
    }
    return sys_load;
}

cJSON *get_status_j()
{
    cJSON *status_j;
    status_j = cJSON_CreateObject();
    
    cJSON_AddNumberToObject(status_j,"sys_uptime",get_sys_uptime());
    cJSON_AddNumberToObject(status_j,"sys_memfree",get_sys_memfree());
    cJSON_AddNumberToObject(status_j,"sys_load",get_sys_load());
    
    long unsigned int wifidog_uptime;
    wifidog_uptime = (long unsigned int)((long unsigned int)time(NULL) - (long unsigned int)started_time);
    cJSON_AddNumberToObject(status_j,"wifidog_uptime",wifidog_uptime); 
    return status_j;

}


char *get_firmware_version()
{
    return "none";
}

char *get_dog_version()
{

	char *version;
	version =get_popen_str("eotu_wifidog -v");
	if(version)
		return version;
	else
		return "eotu_wifidog_v_unknow";

}



cJSON *get_ping_key_id_info()
{
    cJSON *ping_j;
    ping_j = cJSON_CreateObject();
    
    long tt;
    tt = get_current_time();
    get_intervalconfig()->sign = get_new_sign_by_id_gwid_time(get_current_time());
    if(get_intervalconfig()->sign !=NULL)
    	cJSON_AddStringToObject(ping_j,"sign",get_intervalconfig()->sign);

    if(get_intervalconfig()->uid !=NULL)
    	cJSON_AddStringToObject(ping_j,"uid",get_intervalconfig()->uid);

    if(get_intervalconfig()->token !=NULL)
    	cJSON_AddStringToObject(ping_j,"token",get_intervalconfig()->token);

    if(config_get_gsconfig()->router->id >0 )
    	cJSON_AddNumberToObject(ping_j,"id",config_get_gsconfig()->router->id);

    get_intervalconfig()->sign_time = get_current_time();

    cJSON_AddNumberToObject(ping_j,"time",get_intervalconfig()->sign_time);

    return ping_j;
}

cJSON *get_sms_ping_j()
{
    cJSON *ping_j;
    ping_j = cJSON_CreateObject();

    //cJSON_AddItemToObject(ping_j,"router",router_struct_to_j());
    cJSON_AddNumberToObject(ping_j,"id",config_get_gsconfig()->router->id);
    cJSON_AddStringToObject(ping_j,"status","ok");

    return ping_j;
}

/*创建新进程运行cmd*/

char * command_run_thread(char *cmd)
{
    pthread_t tid;
	if(NULL == cmd)
		return "para is NULL, ERROR!";

	int result = pthread_create(&tid, NULL, (void *)excute_cmd, cmd);

	if (result != 0) {
		debug(LOG_ERR, "FATAL: Failed to create a new thread (command_run) - exiting");
		return "Failed to create a new thread";
	}
	pthread_detach(tid);
	return "new thread to run";

}



char *build_sign(char *gw_id,char *key,char *updatetime)
{
    char *sign;
    
    safe_asprintf(&sign,"%s.%s.%s",gw_id,key,updatetime);
    if(NULL == sign)
        return "error";
    return sign;
}



int save_time_to_config_j(cJSON *config_j,char *member,char *string)
{

	if(NULL != cJSON_GetObjectItem(config_j,member))
		cJSON_GetObjectItem(config_j,member)->valuestring = string;
	else
		cJSON_AddStringToObject(config_j,member,print_time());

	return 0;
}



char *get_dongle_imei()
{
	return get_popen_str("uqmi -d /dev/cdc-wdm0 --get-imei");
}

char *get_nano_second()
{
	struct timeval tv;
	char *res;
	gettimeofday(&tv,NULL);

	safe_asprintf(&res,"%d%d",tv.tv_sec,tv.tv_usec);
	return res;
}

char *get_http_res(t_auth_serv *auth_server,int sockfd,char *request )
{
	static int authdown = 0;
	char *res;
#ifdef USE_CYASSL
    if (auth_server->authserv_use_ssl) {
        res = https_get(sockfd, request, auth_server->authserv_hostname);
    } else {
        res = http_get(sockfd, request);
    }
#endif
#ifndef USE_CYASSL
    res = http_get(sockfd, request);
#endif
    return res;
}

char *get_new_sign_by_id_gwid_time(long time)
{
    char *tmp_str;
    safe_asprintf(&tmp_str,"%s%s%d",config_get_gsconfig()->router->gw_id,config_get_gsconfig()->router->key,time);
    tmp_str = str_replace(tmp_str,"(null)","");
    unsigned char decrypt[16];
     MD5_CTX md5;
     MD5Init(&md5);
     MD5Update(&md5,tmp_str,strlen((char *)tmp_str));
     MD5Final(&md5,decrypt);
     int i =0;
     char *mdres="";
     for(;i<16;i++)
         safe_asprintf(&mdres,"%s%02x",mdres,decrypt[i]);
     get_intervalconfig()->sign = mdres;
     debug(LOG_INFO,"r\n\r\n加密前:%s\n加密后:%s\r\n\r\n",tmp_str,mdres);
     return mdres;
}


static int get_document_dir(char *buf, int len)
{
	DIR *d;
	dev_t root;
	struct stat s;
	struct dirent *e;

	if (stat("/", &s))
		return -1;

	if (!(d = opendir("/dev")))
		return -1;

	root = s.st_dev;

	while ((e = readdir(d)) != NULL) {
		snprintf(buf, len, "/dev/%s", e->d_name);

		if (stat(buf, &s) || s.st_rdev != root)
			continue;

		closedir(d);
		return 0;
	}
	closedir(d);
	return -1;
}


/**/
int wifi_ap_member_set(char *member,char *value)
{
	char *cmd;
	safe_asprintf(&cmd,"uci set wireless.apmode.%s=%s",member,value);
	debug(LOG_NOTICE,"%s",cmd);
	return excute_cmd(cmd);
}

/**/
int wifi_sta_member_set(char *member,char *value)
{
	char *cmd;
	safe_asprintf(&cmd,"uci set wireless.stamode.%s=%s",member,value);
	debug(LOG_NOTICE,"%s",cmd);
	return excute_cmd(cmd);
}


int wifi_ap_commit_apply()
{
	debug(LOG_NOTICE,"uci commit wireless");
	return excute_cmd("uci commit wireless");
}


/*更新固件*/
char * upgrade_firmware(cJSON *p)
{
	if(!p)
		return "para is NULl,ERROR!";
	char *cmd;

	//判断固件是否合法
	safe_asprintf(&cmd,"sysupgrade -T %s",p->valuestring);
	debug(LOG_NOTICE,"%s",cmd);
	int rc;
	rc = excute_cmd(cmd);
	if(rc != 0)
		return "firmware is illeggle";

	//杀死必要进程
	debug(LOG_NOTICE,"killall dropbear uhttpd");
	rc = excute_cmd("killall dropbear uhttpd");
	if(rc != 0)
		return "killall dropbear uhttpd Fail";

	//开始升级
	safe_asprintf(&cmd,"sysupgrade -v %s",p->valuestring);
	rc = excute_cmd(cmd);
	debug(LOG_NOTICE,"killall dropbear uhttpd");
	if(rc != 0)
		return "sysupgrade fail";
	return "OK";

}

