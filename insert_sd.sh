#!/bin/sh

PATH="/mnt/mmcblk0p1"
PATH_DEV="/dev/mmcblk0p1"

if [ -b "$PATH_DEV" ] ;then
    if [ ! -d "$PATH" ]; then
        /bin/mkdir -p $PATH
    fi
    /bin/mount -t vfat $PATH_DEV $PATH
fi
