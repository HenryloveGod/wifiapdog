include $(TOPDIR)/rules.mk

PKG_NAME:=eotu_wifidog
PKG_VERSION:=1.0
PKG_RELEASE:=0

PKG_BUILD_DIR := $(BUILD_DIR)/$(PKG_NAME)-$(PKG_VERSION)-$(PKG_RELEASE)/wifidog

PKG_TMP_DIR := $(BUILD_DIR)/$(PKG_NAME)-$(PKG_VERSION)-$(PKG_RELEASE)

PKG_FIXUP = libtool

include $(INCLUDE_DIR)/package.mk

define Package/eotu_wifidog
  SUBMENU:=Captive Portals
  SECTION:=net
  CATEGORY:=Network
  DEPENDS:=+iptables-mod-extra +iptables-mod-ipopt +iptables-mod-nat-extra +libpthread
  TITLE:=eotu_wifidog
  MAINTENER:=D.L
endef


define Package/eotu_wifidog/description
	eotu_wifidog
endef

define Build/Prepare
	mkdir -p $(PKG_BUILD_DIR)
	$(CP) ./wifidog/*  $(PKG_BUILD_DIR)
	$(CP) ./*  $(BUILD_DIR)/$(PKG_NAME)-$(PKG_VERSION)-$(PKG_RELEASE)
endef

define Package/eotu_wifidog/install
	$(INSTALL_DIR) $(1)/usr/bin
	$(INSTALL_DIR) $(1)/etc
	$(INSTALL_DIR) $(1)/etc/init.d/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/eotu_wifidog $(1)/usr/bin/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/wdctl $(1)/usr/bin/
	$(INSTALL_DATA) $(PKG_BUILD_DIR)/wifidog.conf.json $(1)/etc/
	$(INSTALL_DATA) $(PKG_BUILD_DIR)/wifidog.conf.json.local $(1)/etc/
	$(INSTALL_DATA) $(PKG_BUILD_DIR)/wifidog.conf.json.eotu $(1)/etc/
	$(INSTALL_DATA) $(PKG_TMP_DIR)/wifidog-msg.html $(1)/etc/
	$(INSTALL_BIN) $(PKG_TMP_DIR)/eotu_wifidog.sh $(1)/etc/init.d/
	$(INSTALL_BIN) $(PKG_TMP_DIR)/setpwd.lua $(1)/usr/bin/
	$(INSTALL_BIN) $(PKG_TMP_DIR)/insert_sd.sh $(1)/usr/bin/


endef

define Package/eotu_wifidog/postinst

	echo "eotu_wifidog installed"

endef

$(eval $(call BuildPackage,eotu_wifidog))
